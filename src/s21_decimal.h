#ifndef SRC_S21_DECIMAL_H_
#define SRC_S21_DECIMAL_H_

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_N_SCALE 28

// Sign position for (get/set)Bit() function
#define SIGN_POSITION 127
// Scale first bit offset for get/set)Bit() function
#define SCALE_OFFSET 96 + 16

typedef enum {
    s21_NORMAL_VALUE = 0,
    s21_INFINITY = 1,
    s21_NEGATIVE_INFINITY = 2,
    s21_NAN = 3,
    ADDITIONDL_CODE = 4
} value_type_t;

typedef struct {
    unsigned bits[4];
    value_type_t value_type;
} s21_decimal;

enum { LOW, MID, HIGH, SCALE };

typedef enum sign { PLUS = 0, MINUS = 1 } sign_t;

typedef enum output {
    HUMAN_FORMAT = 0,
    MEMORY_FORMAT = 1,
    CONVERT_FORMAT = 2,
} output_t;

enum {
    SUCCESS = 0,
    CONVERTING_ERROR = 1,
};

typedef union {
    float flt;
    struct {
        unsigned int mantisa : 23;
        unsigned int exponent : 8;
        unsigned int sign : 1;
    } parts;
} float_struct;

#define ABS(VALUE) ((VALUE >= 0) ? (VALUE) : (-1 * VALUE))

// Arithmetic operators

s21_decimal s21_add(s21_decimal dec_1, s21_decimal dec_2);
s21_decimal s21_sub(s21_decimal dec_1, s21_decimal dec_2);
s21_decimal s21_mul(s21_decimal dec_1, s21_decimal dec_2);
s21_decimal s21_div(s21_decimal dec_1, s21_decimal dec_2);
s21_decimal s21_mod(s21_decimal dec_1, s21_decimal dec_2);

// Comparsion

int s21_is_less(s21_decimal dec_1, s21_decimal dec_2);
int s21_is_less_or_equal(s21_decimal dec_1, s21_decimal dec_2);
int s21_is_equal(s21_decimal dec_1, s21_decimal dec_2);
int s21_is_not_equal(s21_decimal dec_1, s21_decimal dec_2);
int s21_is_greater_or_equal(s21_decimal dec_1, s21_decimal dec_2);
int s21_is_greater(s21_decimal dec_1, s21_decimal dec_2);

// Internal functions

int getBit(s21_decimal num, int index);
int setBit(s21_decimal* num, int index, int value);
// void printDecimalBits(s21_decimal num, output_t output_type);
int lastSignificantBit(s21_decimal num);
void initDecimal(s21_decimal* num);
int shiftLeft(s21_decimal* num);
int shiftRight(s21_decimal* num);
void set_sign(s21_decimal* num, sign_t sign);
sign_t get_sign(s21_decimal num);
bool is_empty(s21_decimal num);
bool is_scale_overflow(s21_decimal num);
int get_scale(s21_decimal num);
bool set_scale(s21_decimal* num, int n_scale);
bool scale_up(s21_decimal* num);
bool scale_down(s21_decimal* num, int amount, int round);
int alignScales(s21_decimal* num1, s21_decimal* num2);
int cmpDecimals(s21_decimal num1, s21_decimal num2);
s21_decimal divDecimals(s21_decimal dividable, s21_decimal divisor, int mode,
                        s21_decimal* remainder);
int string_to_int(const char* string);
bool validate(s21_decimal src);
bool set_value_type(s21_decimal* dec, int value);
int add_value_types(s21_decimal dec_1, s21_decimal dec_2);
int sub_value_types(s21_decimal dec_1, s21_decimal dec_2);
int div_value_types(s21_decimal dec_1, s21_decimal dec_2);

// Convertors and parsers

int s21_from_float_to_decimal(float src, s21_decimal* dst);
int s21_from_decimal_to_float(s21_decimal src, float* dst);
int s21_from_int_to_decimal(int src, s21_decimal* dst);
int s21_from_decimal_to_int(s21_decimal src, int* dst);
s21_decimal s21_negate(s21_decimal src);
s21_decimal s21_truncate(s21_decimal src);
s21_decimal s21_floor(s21_decimal src);
s21_decimal s21_round(s21_decimal src);

#endif  // SRC_S21_DECIMAL_H_
