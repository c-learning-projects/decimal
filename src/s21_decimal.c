// s21_ fuctions

#include <math.h>
#include <string.h>

#include "s21_decimal.h"

s21_decimal s21_add(s21_decimal num1, s21_decimal num2) {
    int add_value = add_value_types(num1, num2);
    bool v1 = validate(num1);
    bool v2 = validate(num2);
    // validation + add_value check
    if (v1 && v2 && add_value == s21_NORMAL_VALUE) {
        if (get_scale(num1) != get_scale(num2)) {
            alignScales(&num1, &num2);
        }

        int redirected = 0;
        if (get_sign(num1) == MINUS && get_sign(num2) == PLUS) {
            set_sign(&num1, PLUS);
            num1 = s21_sub(num2, num1);
            redirected = 1;
        }
        if (get_sign(num1) == PLUS && get_sign(num2) == MINUS && !redirected) {
            set_sign(&num2, PLUS);
            num1 = s21_sub(num1, num2);
            redirected = 1;
        }
        if (get_sign(num1) == MINUS && get_sign(num2) == MINUS && !redirected) {
            set_sign(&num1, PLUS);
            set_sign(&num2, PLUS);
            num1 = s21_add(num1, num2);
            if (num1.value_type == s21_INFINITY)
                num1.value_type = s21_NEGATIVE_INFINITY;
            else
                set_sign(&num1, MINUS);
            redirected = 1;
        }

        if (!redirected) {
            int aSize = lastSignificantBit(num1);
            int bSize = lastSignificantBit(num2);

            // max(a,b)
            int operationLength = aSize > bSize ? aSize : bSize;
            int tmp = 0;

            int i = 0;

            while (i <= operationLength + 1 || tmp == 1) {
                // overflow check
                if (i >= 96 && tmp == 1) {
                    if (num2.value_type != ADDITIONDL_CODE) {
                        initDecimal(&num1);
                        num1.value_type = s21_INFINITY;
                    }
                    break;
                }

                int amount = 0;
                if (getBit(num1, i)) amount++;
                if (getBit(num2, i)) amount++;
                if (tmp) {
                    amount++;
                    tmp = 0;
                }
                switch (amount) {
                    case 0:
                        break;
                    case 1:
                        setBit(&num1, i, 1);
                        break;
                    case 2:
                        setBit(&num1, i, 0);
                        tmp = 1;
                        break;
                    case 3:
                        setBit(&num1, i, 1);
                        tmp = 1;
                        break;
                }
                i++;
            }

            //  if this is a sub
            if (num2.value_type == ADDITIONDL_CODE) {
                // if overflow happened
                if (i >= 96 && tmp == 1) {
                    num1.value_type = ADDITIONDL_CODE;
                }
            }
        }
    } else {
        if (!v1) set_value_type(&num1, s21_NAN);
        if (!v2) set_value_type(&num2, s21_NAN);
        if (!v2 || !v1) set_value_type(&num1, s21_NAN);
        if (add_value != s21_NORMAL_VALUE) set_value_type(&num1, add_value);
    }
    return num1;
}

s21_decimal s21_sub(s21_decimal num1, s21_decimal num2) {
    s21_decimal result;
    initDecimal(&result);
    int add_value = sub_value_types(num1, num2);
    bool v1 = validate(num1);
    bool v2 = validate(num2);
    // validation + add_value chack
    if (v1 && v2 && add_value == s21_NORMAL_VALUE) {
        if (get_scale(num1) != get_scale(num2)) {
            alignScales(&num1, &num2);
        }

        int redirected = 0;
        if (get_sign(num1) == MINUS && get_sign(num2) == PLUS) {
            set_sign(&num1, PLUS);
            result = s21_add(num2, num1);
            if (result.value_type == s21_INFINITY) {
                initDecimal(&result);  // just make sure
                result.value_type = s21_NEGATIVE_INFINITY;
            }
            set_sign(&result, MINUS);
            redirected = 1;
        }
        if (get_sign(num1) == PLUS && get_sign(num2) == MINUS && !redirected) {
            set_sign(&num2, PLUS);
            result = s21_add(num1, num2);
            redirected = 1;
        }
        if (get_sign(num1) == MINUS && get_sign(num2) == MINUS && !redirected) {
            set_sign(&num1, PLUS);
            set_sign(&num2, PLUS);
            result = s21_sub(num2, num1);
            redirected = 1;
        }

        if (!redirected) {
            int negative =
                s21_is_less(num1, num2);  // (!) jdreama invert s21_is_less (!)

            int aSize = lastSignificantBit(num1);
            int bSize = lastSignificantBit(num2);

            // max(a,b)
            int invertLen = aSize > bSize ? aSize : bSize;

            for (int i = invertLen; i >= 0; i--) {
                setBit(&num2, i, getBit(num2, i) ^ 1);
            }

            num2.value_type = ADDITIONDL_CODE;

            result = s21_add(num1, num2);

            // if overflow happened
            if (result.value_type == ADDITIONDL_CODE) {
                result.value_type = s21_NORMAL_VALUE;
                s21_decimal one;
                initDecimal(&one);
                one.bits[LOW] = 1;
                set_scale(&one, get_scale(result));
                result = s21_add(result, one);
            } else {
                // if no overflow, but number become longer
                if (lastSignificantBit(result) > invertLen) {
                    setBit(&result, lastSignificantBit(result), 0);
                    s21_decimal one;
                    initDecimal(&one);
                    one.bits[LOW] = 1;
                    set_scale(&one, get_scale(result));
                    result = s21_add(result, one);
                } else {
                    for (int i = invertLen; i >= 0; i--) {
                        setBit(&result, i, getBit(result, i) ^ 1);
                    }
                }
            }

            if (negative == 0) {
                set_sign(&result, MINUS);
            } else {
                set_sign(&result, PLUS);
            }
        }
    } else {
        if (!v1) set_value_type(&num1, s21_NAN);
        if (!v2) set_value_type(&num2, s21_NAN);
        if (!v2 || !v1) set_value_type(&result, s21_NAN);
        if (add_value != s21_NORMAL_VALUE) set_value_type(&result, add_value);
    }

    return result;
}

// decimal multiplication
s21_decimal s21_mul(s21_decimal dec_1, s21_decimal dec_2) {
    s21_decimal dec_sum;
    s21_decimal dec_1tmp;
    initDecimal(&dec_sum);
    initDecimal(&dec_1tmp);
    bool v1 = 0;
    bool v2 = 0;
    if ((v1 = validate(dec_1)) && (v2 = validate(dec_2))) {
        // checks for value_types
        int value1 = dec_1.value_type;
        int value2 = dec_2.value_type;
        if (value1 || value2) {
            if ((value1 == s21_INFINITY && value2 == s21_INFINITY) ||
                (value1 == s21_NEGATIVE_INFINITY &&
                 value2 == s21_NEGATIVE_INFINITY) ||
                (!value1 && get_sign(dec_1) &&
                 value2 == s21_NEGATIVE_INFINITY) ||
                (!value2 && get_sign(dec_2) &&
                 value1 == s21_NEGATIVE_INFINITY) ||
                (!value1 && !get_sign(dec_1) && value2 == s21_INFINITY) ||
                (!value2 && !get_sign(dec_2) && value1 == s21_INFINITY))
                dec_sum.value_type = s21_INFINITY;

            if ((!value1 && !get_sign(dec_1) &&
                 value2 == s21_NEGATIVE_INFINITY) ||
                (!value2 && !get_sign(dec_2) &&
                 value1 == s21_NEGATIVE_INFINITY) ||
                (!value1 && get_sign(dec_1) && value2 == s21_INFINITY) ||
                (!value2 && get_sign(dec_2) && value1 == s21_INFINITY))
                dec_sum.value_type = s21_NEGATIVE_INFINITY;

            if ((value1 == s21_INFINITY && !value2 && is_empty(dec_2)) ||
                (value1 == s21_NEGATIVE_INFINITY && !value2 &&
                 is_empty(dec_2)) ||
                (value2 == s21_INFINITY && !value1 && is_empty(dec_1)) ||
                (value2 == s21_NEGATIVE_INFINITY && !value1 &&
                 is_empty(dec_1)) ||
                (value1 == s21_NAN || value2 == s21_NAN) ||
                (value1 == s21_NEGATIVE_INFINITY && value2 == s21_INFINITY) ||
                (value2 == s21_NEGATIVE_INFINITY && value1 == s21_INFINITY))
                dec_sum.value_type = s21_NAN;

        } else {
            // decimal multiplication block
            alignScales(&dec_1, &dec_2);
            dec_1tmp = dec_1;
            set_sign(&dec_1tmp, PLUS);  // for s_21_add working right
            for (int i = 0; i < 96; i++) {
                if (getBit(dec_2, i))                      // decimal*0 = 0
                    dec_sum = s21_add(dec_sum, dec_1tmp);  // add non 0 only
                shiftLeft(&dec_1tmp);  // shift left every step
            }

            // result scale = scale 1 + scale 2
            int scale = get_scale(dec_1) + get_scale(dec_2);
            set_scale(&dec_sum, scale);

            // sign block
            sign_t sign1 = get_sign(dec_1);
            sign_t sign2 = get_sign(dec_2);
            sign_t sign = ((sign1 && !sign2) || (!sign1 && sign2));
            set_sign(&dec_sum, sign);
        }
        int values = dec_sum.value_type;
        if (values) {
            int mInf = get_sign(dec_sum);
            initDecimal(&dec_sum);  // decimal = 0

            if (values == s21_INFINITY) {
                if (mInf == MINUS)
                    dec_sum.value_type = s21_NEGATIVE_INFINITY;
                else
                    dec_sum.value_type = s21_INFINITY;
            }

            if (values == s21_NEGATIVE_INFINITY) {
                if (mInf == MINUS)
                    dec_sum.value_type = s21_INFINITY;
                else
                    dec_sum.value_type = s21_NEGATIVE_INFINITY;
            }

            if (values == s21_NAN) dec_sum.value_type = s21_NAN;
        }

    } else {
        if (!v1) set_value_type(&dec_1, s21_NAN);
        if (!v2) set_value_type(&dec_2, s21_NAN);
        set_value_type(&dec_sum, s21_NAN);
    }
    return dec_sum;
}

s21_decimal s21_div(s21_decimal dec_1, s21_decimal dec_2) {
    s21_decimal dec_res;
    initDecimal(&dec_res);
    int add_value = div_value_types(dec_1, dec_2);
    bool v1 = validate(dec_1);
    bool v2 = validate(dec_2);
    // printf("1:v1 = %d, v2 = %d, add_value = %d\n", v1, v2, add_value);

    // validation + add_value chack
    if (v1 && v2 && add_value == s21_NORMAL_VALUE) {
        dec_res = divDecimals(dec_1, dec_2, 2, NULL);
    } else {
        if (!v1) set_value_type(&dec_1, s21_NAN);
        if (!v2) set_value_type(&dec_2, s21_NAN);
        if (!v2 || !v1) set_value_type(&dec_res, s21_NAN);
        if (add_value != s21_NORMAL_VALUE) set_value_type(&dec_res, add_value);
        // if we have x/inf = 0 (value = 4 means ZERO here)
        if (add_value == 4) set_value_type(&dec_res, s21_NORMAL_VALUE);
    }
    // printf("2: v1 = %d, v2 = %d, add_value = %d, res_value = %d\n", v1, v2,
    //    add_value, dec_res.value_type);
    return dec_res;
}

s21_decimal s21_mod(s21_decimal dec_1, s21_decimal dec_2) {
    s21_decimal remainder;
    initDecimal(&remainder);
    int add_value = div_value_types(dec_1, dec_2);
    bool v1 = validate(dec_1);
    bool v2 = validate(dec_2);
    // validation + add_value chack
    if (v1 && v2 && add_value == s21_NORMAL_VALUE) {
        alignScales(&dec_1, &dec_2);
        divDecimals(dec_1, dec_2, 0, &remainder);
    } else {
        if (!v1) set_value_type(&dec_1, s21_NAN);
        if (!v2) set_value_type(&dec_2, s21_NAN);
        if (!v2 || !v1) set_value_type(&remainder, s21_NAN);
        if (add_value != s21_NORMAL_VALUE)
            set_value_type(&remainder, add_value);
        // if we have x/inf = 0 (value = 4 means ZERO here)
        if (add_value == 4) set_value_type(&remainder, s21_NORMAL_VALUE);
    }
    return remainder;
}

#define BUF_LEN 32

/**
 * @brief convert single floating point number to decimal number
 * @param src float number
 * @param dst pointer to the decimal number
 * @returns function returns conversion status code
 * @retval 0 - SUCCESS
 * @retval 1 - CONVERTING_ERROR
 */
int s21_from_float_to_decimal(float src, s21_decimal *dst) {
    int convert_status;

    float_struct float_structure;
    s21_decimal temp_res;
    s21_decimal ten;
    initDecimal(&ten);
    s21_from_int_to_decimal(10, &ten);

    char str_num[BUF_LEN];
    char str_buf[BUF_LEN];
    memset(str_num, 0, BUF_LEN);
    memset(str_buf, 0, BUF_LEN);

    int true_exp_scale;
    int exp_scale;
    int i;

    float_structure.flt = src;
    if (dst != NULL) {
        initDecimal(dst);
    } else {
        convert_status = CONVERTING_ERROR;
    }

    // if exponent <= 222 nunmber will fit in decimal
    // real exponent = 222 - 127 = 95, but float has a hidden one
    // that mean 222 - 127 + 1 BUT if number denormalized
    // real exponent = 0 and float has not a hidden one
    if (float_structure.parts.exponent <= 222 && dst != NULL) {
        convert_status = SUCCESS;
        // get float string representation
        snprintf(str_buf, BUF_LEN, "%+.6e", src);

        // str_buf after snprintf = (digit)(dot)(six
        // digits)(e)(sign)(exponent size) find (e) character + 1 and
        // transform exponent (sign)(exponent size) to int
        exp_scale = string_to_int((strchr(str_buf, 'e') + 1));

        // if not all digits fit exp[-21:254]
        if (exp_scale < -22) {
            // if number is too small
            if (28 - ABS(exp_scale) < -1) {
                str_num[0] = '+' + 2 * float_structure.parts.sign;
                str_num[1] = '0';
                str_num[2] = '\0';
                i = 2;
                // if number overflow
            } else if (28 - ABS(exp_scale) < 0) {
                snprintf(str_buf, BUF_LEN, "%+.1e", src);
                str_num[0] = '+' + 2 * float_structure.parts.sign;
                str_num[1] = (str_buf[1] >= '5') ? '1' : '0';
                str_num[2] = '\0';
                ++exp_scale;
                i = 2;
                // if only one digit of number can fit
            } else if (28 - ABS(exp_scale) == 0) {
                snprintf(str_buf, BUF_LEN, "%+.0e", src);
                str_num[0] = '+' + 2 * float_structure.parts.sign;
                str_num[1] = str_buf[1];
                str_num[2] = '\0';
                i = 2;
                // if not all ditis can fit
            } else {
                snprintf(str_num, BUF_LEN, "%+.*e", 28 - ABS(exp_scale), src);
                i = 2;
                while (str_num[i + 1] != 'e') {
                    str_num[i] = str_num[i + 1];
                    ++i;
                }
                str_num[i] = '\0';
            }
            // if all digit cat fit
        } else {
            snprintf(str_num, BUF_LEN, "%+.6e", src);
            i = 2;
            while (str_num[i + 1] != 'e') {
                str_num[i] = str_num[i + 1];
                ++i;
            }
            str_num[i] = '\0';
        }

        // after all i points to '\0'
        i -= 1;
        // remove trailing zeroes
        while (str_num[i] == '0' &&
               (str_num + (i)) > (str_num + 1 + exp_scale)) {
            str_num[i] = 0;
            i--;
        }

        s21_from_int_to_decimal(string_to_int(str_num), dst);

        if ((exp_scale - ((int)strlen(str_num) - 2) - 1) > 0) {
            true_exp_scale = exp_scale - (strlen(str_num) - 1);
            i = 0;
            while (i != true_exp_scale + 1) {
                temp_res = s21_mul(*dst, ten);
                memcpy(dst, &temp_res, sizeof(s21_decimal));
                ++i;
            }
            set_scale(dst, 0);
        } else {
            true_exp_scale = (exp_scale - (strlen(str_num) - 1));
            set_scale(dst, -1 * (true_exp_scale + 1));
        }

    } else {
        convert_status = CONVERTING_ERROR;

        if (isinf(float_structure.flt)) {
            if (float_structure.parts.sign == PLUS) {
                dst->value_type = s21_INFINITY;
            } else {
                dst->value_type = s21_NEGATIVE_INFINITY;
            }
        } else if (isnan(float_structure.flt)) {
            dst->value_type = s21_NAN;
        }
    }

    return convert_status;
}

#undef BUF_LEN

/**
 * @brief convert from decimal number to single floating point number
 * @param src copy of float number
 * @param dst decimal number
 * @return function returns conversion status code
 * @retval 0 - SUCCESS
 * @retval 1 - CONVERTING_ERROR
 */
int s21_from_decimal_to_float(s21_decimal src, float *dst) {
    int convert_status;
    int scale;
    double divisor = 1.0;
    double high_part;
    double mid_part;
    if (validate(src)) {
        if (dst != NULL && src.value_type == s21_NORMAL_VALUE) {
            convert_status = SUCCESS;

            scale = get_scale(src);
            divisor = 1.0;
            while (scale-- != 0) {
                divisor *= 10.0;
            }

            if (src.bits[MID] != 0) {
                mid_part = (double)src.bits[MID] / divisor;
                mid_part *= 65536.0;
                mid_part *= 65536.0;
            } else {
                mid_part = 0.0f;
            }

            if (src.bits[HIGH] != 0) {
                high_part = (double)src.bits[HIGH] / divisor;
                high_part *= 4294967296.0;
                high_part *= 4294967296.0;
            } else {
                high_part = 0.0f;
            }

            *dst = (double)src.bits[LOW] / divisor + high_part + mid_part;

            if (get_sign(src) == MINUS) {
                *dst = *dst * (1.0 - 2 * get_sign(src));
            }
        } else {
            convert_status = CONVERTING_ERROR;
        }
    } else {
        set_value_type(&src, s21_NAN);
        convert_status = CONVERTING_ERROR;
    }
    return convert_status;
}

// int to decimal
int s21_from_int_to_decimal(int src, s21_decimal *dst) {
    initDecimal(dst);
    int error = 0;
    dst->bits[LOW] = abs(src);
    if (src < 0) set_sign(dst, MINUS);
    return error;
}

int s21_from_decimal_to_int(s21_decimal src, int *dst) {
    int convert_status = SUCCESS;
    if (validate(src)) {
        int value = src.value_type;
        if (value == s21_NORMAL_VALUE) {
            // scale down dec to 0
            s21_decimal src_tmp = src;
            int nscale = get_scale(src_tmp);
            if (nscale > 0) scale_down(&src_tmp, nscale, 0);

            int decMinus = getBit(src_tmp, 127);
            if (!convert_status && !src_tmp.bits[MID] && !src_tmp.bits[HIGH] &&
                !src_tmp.value_type) {
                *dst = src_tmp.bits[LOW];
                if (decMinus) *dst *= -1;

            } else {
                convert_status = CONVERTING_ERROR;
                *dst = 0;
            }
        } else {
            set_value_type(&src, value);
            convert_status = CONVERTING_ERROR;
        }
    } else {
        set_value_type(&src, s21_NAN);
        convert_status = CONVERTING_ERROR;
    }
    return convert_status;
}

// decimal x -1
s21_decimal s21_negate(s21_decimal src) {
    if (validate(src)) {
        int value = src.value_type;
        if (value == s21_NORMAL_VALUE) {
            if (get_sign(src) == PLUS)
                set_sign(&src, MINUS);
            else  // if (!is_empty(src))  // 0*-1 = 0
                set_sign(&src, PLUS);
        } else {
            initDecimal(&src);
            if (value == s21_NEGATIVE_INFINITY)
                src.value_type = s21_INFINITY;  // ???
            if (value == s21_INFINITY) src.value_type = s21_NEGATIVE_INFINITY;
            if (value == s21_NAN) src.value_type = s21_NAN;
        }
    } else {
        initDecimal(&src);
        src.value_type = s21_NAN;
    }
    return src;
}

s21_decimal s21_truncate(s21_decimal src) {
    s21_decimal src_tmp;
    initDecimal(&src_tmp);
    if (validate(src)) {
        int scale = get_scale(src);
        if (scale > 0) {
            src_tmp = src;
            // printDecimalBits(src_tmp, HUMAN_FORMAT);
            scale_down(&src_tmp, scale, 0);
            // printDecimalBits(src_tmp, HUMAN_FORMAT);
        }
        set_sign(&src_tmp, get_sign(src));

        if (src_tmp.value_type != s21_NORMAL_VALUE) {
            set_value_type(&src_tmp, src_tmp.value_type);
        }
    } else {
        set_value_type(&src, s21_NAN);
        set_value_type(&src_tmp, s21_NAN);
    }
    if (src.value_type != s21_NORMAL_VALUE) {
        set_value_type(&src_tmp, src.value_type);
    }
    return src_tmp;
}

s21_decimal s21_round(s21_decimal src) {
    sign_t sign = get_sign(src);
    s21_decimal src_tmp = src;
    s21_decimal tail;
    initDecimal(&tail);
    s21_decimal half;
    initDecimal(&half);
    if (validate(src)) {
        // put decimal to compare = 0.5
        half.bits[LOW] = 5;
        set_scale(&half, 1);

        // scaling down to 0 to get int from src
        src_tmp = s21_truncate(src);

        // tail after dot = (src - integer part of the number src (or
        // antier))
        set_sign(&src, PLUS);
        set_sign(&src_tmp, PLUS);
        tail = s21_sub(src, src_tmp);
        set_sign(&tail, PLUS);

        // if  0.5 <= tail add 1 to result
        if (s21_is_less_or_equal(half, tail) == 0) {
            initDecimal(&tail);
            tail.bits[LOW] = 1;
        } else {
            initDecimal(&tail);
        }

        // add 1 or -1 to result and return signs
        src_tmp = s21_add(src_tmp, tail);
        set_sign(&src, sign);
        set_sign(&src_tmp, sign);
        set_sign(&tail, sign);

        // if there are no bits, delete sign
        // if (is_empty(src_tmp)) set_sign(&src_tmp, PLUS);

        if (src_tmp.value_type != s21_NORMAL_VALUE) {
            set_value_type(&src_tmp, src_tmp.value_type);
        }
    } else {
        set_value_type(&src, s21_NAN);
        set_value_type(&src_tmp, s21_NAN);
    }
    if (src.value_type != s21_NORMAL_VALUE) {
        set_value_type(&src_tmp, src.value_type);
    }
    return src_tmp;
}

s21_decimal s21_floor(s21_decimal src) {
    sign_t sign = get_sign(src);
    s21_decimal src_tmp = src;
    s21_decimal minus_1;
    initDecimal(&minus_1);
    minus_1.bits[LOW] = 1;
    set_sign(&minus_1, MINUS);
    if (validate(src)) {
        src_tmp = s21_truncate(src);
        if ((sign == MINUS) && get_scale(src) > 0) {
            src_tmp = s21_add(src_tmp, minus_1);
        }

        // if (is_empty(src_tmp)) set_sign(&src_tmp, PLUS);
        if (src_tmp.value_type != s21_NORMAL_VALUE) {
            set_value_type(&src_tmp, src_tmp.value_type);
        }
    } else {
        set_value_type(&src, s21_NAN);
        set_value_type(&src_tmp, s21_NAN);
    }
    if (src.value_type != s21_NORMAL_VALUE) {
        set_value_type(&src_tmp, src.value_type);
    }
    return src_tmp;
}

int s21_is_less(s21_decimal num1, s21_decimal num2) {
    bool v1 = 0;
    bool v2 = 0;
    int result = -1;
    int value1 = num1.value_type;
    int value2 = num2.value_type;
    bool skip = false;
    if ((value1 + value2) != 0) skip = true;
    if ((v1 = validate(num1)) && (v2 = validate(num2)) && skip == false) {
        if (get_scale(num1) != get_scale(num2)) {
            alignScales(&num1, &num2);
        }

        // if signs are different
        if (get_sign(num1) != get_sign(num2)) {
            if (get_sign(num1) == MINUS)
                result = 1;
            else
                result = 0;
        }

        if (result == -1) {
            for (int i = 95; i >= 0; i--) {
                if (!getBit(num1, i) && getBit(num2, i)) {
                    result = 1;
                    break;
                }
                if (getBit(num1, i) && !getBit(num2, i)) {
                    result = 0;
                    break;
                }
            }
            if (result == -1) result = 0;
            // if numbers are neganive, invert result
            else if (get_sign(num1) == MINUS && get_sign(num2) == MINUS)
                result = 1 ^ result;
        }
    } else {
        result = false;
    }
    return !result;
}

int s21_is_less_or_equal(s21_decimal dec_1, s21_decimal dec_2) {
    int result = 1;
    if (!s21_is_equal(dec_1, dec_2) || !s21_is_less(dec_1, dec_2)) result = 0;
    return result;
}

int s21_is_greater_or_equal(s21_decimal dec_1, s21_decimal dec_2) {
    int result = 1;
    if (!s21_is_equal(dec_1, dec_2) || !s21_is_greater(dec_1, dec_2))
        result = 0;
    return result;
}

int s21_is_greater(s21_decimal dec_1, s21_decimal dec_2) {
    int result = 1;
    if ((s21_is_less(dec_1, dec_2) == 1) && (s21_is_equal(dec_1, dec_2) == 1))
        result = 0;
    return result;
}

int s21_is_equal(s21_decimal dec_1, s21_decimal dec_2) {
    int equal = 0;
    bool v1 = validate(dec_1);
    bool v2 = validate(dec_2);
    int value1 = dec_1.value_type;
    int value2 = dec_2.value_type;
    bool skip = false;
    if ((value1 + value2) != 0) skip = true;
    if (v1 && v2 && skip == false) {
        alignScales(&dec_1, &dec_2);
        for (int i = 0; i < 4; i++)
            // compare bits
            if (dec_1.bits[i] != dec_2.bits[i]) equal = 1;
    } else {
        equal = 1;
    }
    return equal;
}

int s21_is_not_equal(s21_decimal dec_1, s21_decimal dec_2) {
    return !s21_is_equal(dec_1, dec_2);
}
