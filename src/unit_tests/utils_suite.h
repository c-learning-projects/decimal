#ifndef SRC_UNIT_TESTS_UTILS_SUITE_H_
#define SRC_UNIT_TESTS_UTILS_SUITE_H_

#include <check.h>

#include "s21_decimal.h"

Suite *utils_suite(void);

#endif  // SRC_UNIT_TESTS_UTILS_SUITE_H_
