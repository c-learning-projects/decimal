#include "decimal_suite.h"

#define FREE_VAR(pVAR)      \
    {                       \
        if (pVAR != NULL) { \
            free(pVAR);     \
        }                   \
    }

#define EPS 1e-16

START_TEST(s21_from_float_to_decimal_normal_case) {
    s21_decimal *num1 = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal *num2 = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal temp;

    s21_from_float_to_decimal(0.0, num1);
    s21_from_int_to_decimal(0, num2);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(50.0, num1);
    s21_from_int_to_decimal(50, num2);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(50.5, num1);
    s21_from_int_to_decimal(505, num2);
    set_scale(num2, 1);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(5e-29, num1);
    s21_from_int_to_decimal(1, num2);
    set_scale(num2, 28);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(5.5e-28, num1);
    s21_from_int_to_decimal(5, num2);
    set_scale(num2, 28);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(5.55e-28, num1);
    s21_from_int_to_decimal(6, num2);
    set_scale(num2, 28);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(400000000.5000, num1);
    s21_from_int_to_decimal(400000000, num2);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(400000000.5000, num1);
    s21_from_int_to_decimal(400000000, num2);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(7.922816e+28, num1);
    s21_from_int_to_decimal(7922816, num2);
    s21_from_float_to_decimal(1e+22, &temp);
    temp = s21_mul(*num2, temp);
    ck_assert_int_eq(s21_is_equal(*num1, temp), 0);

    s21_from_float_to_decimal(7.555555e-27, num1);
    s21_from_int_to_decimal(76, num2);
    set_scale(num2, 28);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(7.92281601530810e-29, num1);
    s21_from_int_to_decimal(1, num2);
    set_scale(num2, 28);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    s21_from_float_to_decimal(5.5555555e-22, num1);
    s21_from_int_to_decimal(5555555, num2);
    set_scale(num2, 28);
    ck_assert_int_eq(s21_is_equal(*num1, *num2), 0);

    FREE_VAR(num1);
    FREE_VAR(num2);
}
END_TEST

START_TEST(s21_from_float_to_decimal_extra_input_case) {
    s21_decimal *num1 = (s21_decimal *)malloc(sizeof(s21_decimal));

    ck_assert_int_eq(s21_from_float_to_decimal(7.92281601530810e+28, num1),
                     SUCCESS);

    ck_assert_int_eq(s21_from_float_to_decimal(7.9228169e+28, num1),
                     CONVERTING_ERROR);

    ck_assert_int_eq(s21_from_float_to_decimal(7.92281601530810e-30, num1),
                     SUCCESS);

    ck_assert_int_eq(s21_from_float_to_decimal(0, NULL), CONVERTING_ERROR);

    ck_assert_int_eq(s21_from_float_to_decimal(INFINITY, num1), CONVERTING_ERROR);
    ck_assert_int_eq(num1->value_type, s21_INFINITY);

    ck_assert_int_eq(s21_from_float_to_decimal(-INFINITY, num1), CONVERTING_ERROR);
    ck_assert_int_eq(num1->value_type, s21_NEGATIVE_INFINITY);

    ck_assert_int_eq(s21_from_float_to_decimal(NAN, num1), CONVERTING_ERROR);
    ck_assert_int_eq(num1->value_type, s21_NAN);

    FREE_VAR(num1);
}
END_TEST

START_TEST(s21_from_decimal_to_float_normal_case) {
    s21_decimal *num1 = (s21_decimal *)malloc(sizeof(s21_decimal));
    int convert_status;
    float flt_in;
    float flt_out;

    flt_in = INFINITY;
    s21_from_float_to_decimal(flt_in, num1);
    convert_status = s21_from_decimal_to_float(*num1, &flt_out);
    ck_assert_int_eq(convert_status, CONVERTING_ERROR);

    flt_in = -INFINITY;
    s21_from_float_to_decimal(flt_in, num1);
    convert_status = s21_from_decimal_to_float(*num1, &flt_out);
    ck_assert_int_eq(convert_status, CONVERTING_ERROR);

    flt_in = NAN;
    s21_from_float_to_decimal(flt_in, num1);
    convert_status = s21_from_decimal_to_float(*num1, &flt_out);
    ck_assert_int_eq(convert_status, CONVERTING_ERROR);

    convert_status = s21_from_decimal_to_float(*num1, NULL);
    ck_assert_int_eq(convert_status, CONVERTING_ERROR);

    flt_in = 5.5;
    s21_from_float_to_decimal(flt_in, num1);
    convert_status = s21_from_decimal_to_float(*num1, &flt_out);
    ck_assert_msg(((flt_in - flt_out) < EPS),
                  "Assertion: failed: s21_from_float_to_decimal(5.5) != "
                  "s21_from_decimal_to_float()");
    ck_assert_int_eq(convert_status, SUCCESS);

    flt_in = 0.0;
    s21_from_float_to_decimal(flt_in, num1);
    convert_status = s21_from_decimal_to_float(*num1, &flt_out);
    ck_assert_msg(((flt_in - flt_out) < EPS),
                  "Assertion: failed: s21_from_float_to_decimal(5.5) != "
                  "s21_from_decimal_to_float()");
    ck_assert_int_eq(convert_status, SUCCESS);

    flt_in = 1e-28;
    s21_from_float_to_decimal(flt_in, num1);
    convert_status = s21_from_decimal_to_float(*num1, &flt_out);
    ck_assert_msg(((flt_in - flt_out) < EPS),
                  "Assertion: failed: s21_from_float_to_decimal(5.5) != "
                  "s21_from_decimal_to_float()");
    ck_assert_int_eq(convert_status, SUCCESS);

    flt_in = 1e+28;
    s21_from_float_to_decimal(flt_in, num1);
    convert_status = s21_from_decimal_to_float(*num1, &flt_out);
    ck_assert_msg(((flt_in - flt_out) < EPS),
                  "Assertion: failed: s21_from_float_to_decimal(5.5) != "
                  "s21_from_decimal_to_float()");
    ck_assert_int_eq(convert_status, SUCCESS);

    FREE_VAR(num1);
}
END_TEST

START_TEST(s21_mul_extra_case) {
    s21_decimal *num1 = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal *num2 = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal *result = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal *expected_result = (s21_decimal *)malloc(sizeof(s21_decimal));

    // !test 1
    s21_from_float_to_decimal(55.55, num1);
    s21_from_float_to_decimal(-5555.55, num2);
    *result = s21_mul(*num1, *num2);

    // expected init, binary values has got with c#
    initDecimal(expected_result);
    expected_result->bits[LOW] = 0b10110111111100100100010101111001;
    set_scale(expected_result, 4);
    set_sign(expected_result, MINUS);

    // comparison
    ck_assert_msg(s21_is_equal(*result, *expected_result) == 0,
                  "Assertion: failed: s21_from_float_to_decimal(5.5) != "
                  "s21_from_decimal_to_float()");
    ck_assert_int_eq(result->value_type, s21_NORMAL_VALUE);

    // !test 2
    s21_from_float_to_decimal(55.55e+20, num1);
    s21_from_float_to_decimal(-5555.55e+20, num2);
    *result = s21_mul(*num1, *num2);

    // comparison
    ck_assert_int_eq(result->value_type, s21_NEGATIVE_INFINITY);

    // !test 3
    s21_from_float_to_decimal(55.55e+13, num1);
    s21_from_float_to_decimal(-5555.55e+10, num2);
    *result = s21_mul(*num1, *num2);

    // expected init, binary values has got with c#
    initDecimal(expected_result);
    expected_result->bits[LOW] = 0b10110110101010000000000000000000;
    expected_result->bits[MID] = 0b10011101011011101111101110100000;
    expected_result->bits[HIGH] = 0b01100011101101111010111111110000;
    set_scale(expected_result, 0);
    set_sign(expected_result, MINUS);

    // comparison
    ck_assert_msg(s21_is_equal(*result, *expected_result) == 0,
                  "Assertion: failed: s21_from_float_to_decimal(5.5) != "
                  "s21_from_decimal_to_float()");
    ck_assert_int_eq(result->value_type, s21_NORMAL_VALUE);

    // !test 4
    s21_from_float_to_decimal(-55.55e+13, num1);
    s21_from_float_to_decimal(-5555.55e+10, num2);
    *result = s21_mul(*num1, *num2);

    // expected init, binary values has got with c#
    set_sign(expected_result, PLUS);

    // comparison
    ck_assert_msg(s21_is_equal(*result, *expected_result) == 0,
                  "Assertion: failed: s21_from_float_to_decimal(5.5) != "
                  "s21_from_decimal_to_float()");
    ck_assert_int_eq(result->value_type, s21_NORMAL_VALUE);

    // !test 5
    s21_from_float_to_decimal(100e+12, num1);
    s21_from_float_to_decimal(789999.99e+9, num2);
    *result = s21_mul(*num1, *num2);

    // expected init, binary values has got with c#
    initDecimal(expected_result);
    expected_result->bits[LOW] = 0b10011000000000000000000000000000;
    expected_result->bits[MID] = 0b01010001010101111001001011001011;
    expected_result->bits[HIGH] = 0b11111111010000110100010010110101;
    set_scale(expected_result, 0);
    set_sign(expected_result, PLUS);

    // !test 6
    s21_from_float_to_decimal(100e+12, num1);
    s21_from_float_to_decimal(1e-20, num2);
    // printDecimalBits(*num1, HUMAN_FORMAT);
    // printDecimalBits(*num2, HUMAN_FORMAT);
    *result = s21_mul(*num1, *num2);

    // expected init, binary values has got with c#
    initDecimal(expected_result);
    expected_result->bits[LOW] = 0b10100100110001101000000000000000;
    expected_result->bits[MID] = 0b00000000000000111000110101111110;
    expected_result->bits[HIGH] = 0b00000000000000000000000000000000;
    set_scale(expected_result, 0b10101);
    set_sign(expected_result, PLUS);

    // alignScales(num1, num2);
    // printDecimalBits(*num1, HUMAN_FORMAT);
    // printDecimalBits(*num2, HUMAN_FORMAT);
    // printDecimalBits(*result, HUMAN_FORMAT);
    // printDecimalBits(*expected_result, HUMAN_FORMAT);
    // printf(" !result value_type = %d \n", result->value_type);

    // comparison
    // doesnt work with overflow decimal !!!
    // ck_assert_msg(s21_is_equal(*result, *expected_result) == 0,
    //               "Assertion: failed: s21_from_float_to_decimal(5.5) != "
    //               "s21_from_decimal_to_float()");
    ck_assert_int_eq(result->value_type, s21_NORMAL_VALUE);

    FREE_VAR(num1);
    FREE_VAR(num2);
    FREE_VAR(result);
    FREE_VAR(expected_result);
}
END_TEST

START_TEST(s21_mul_invalid_value_case) {
    s21_decimal *num1 = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal *num2 = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal *result = (s21_decimal *)malloc(sizeof(s21_decimal));

    // !test 1 inf * 0
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_INFINITY;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    // !test 2 inf * -0
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_INFINITY;
    set_sign(num2, MINUS);
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    // !test 3 inf * nan
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_INFINITY;
    num2->value_type = s21_NAN;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    // !test 4 -inf * nan
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_NEGATIVE_INFINITY;
    num2->value_type = s21_NAN;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    // !test 5 inf * inf
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_INFINITY;
    num2->value_type = s21_INFINITY;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_INFINITY);

    // !test 6 inf * -inf
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_INFINITY;
    num2->value_type = s21_NEGATIVE_INFINITY;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    // !test 7 -inf * -inf
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_NEGATIVE_INFINITY;
    num2->value_type = s21_NEGATIVE_INFINITY;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_INFINITY);

    // !test 8 inf * bigval
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_INFINITY;
    s21_from_float_to_decimal(7e+28, num2);
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_INFINITY);

    // !test 9 inf * -bigval
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_INFINITY;
    s21_from_float_to_decimal(-7e+28, num2);
    *result = s21_mul(*num1, *num2);
    // printDecimalBits(*num2, HUMAN_FORMAT);
    ck_assert_int_eq(result->value_type, s21_NEGATIVE_INFINITY);

    // !test 10 -inf * bigval
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_NEGATIVE_INFINITY;
    s21_from_float_to_decimal(7e+28, num2);
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NEGATIVE_INFINITY);

    // !test 11 -inf * -bigval
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_NEGATIVE_INFINITY;
    s21_from_float_to_decimal(-7e+28, num2);
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_INFINITY);

    // !test 12 values * TRASH
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_NEGATIVE_INFINITY;
    num2->bits[SCALE] = UINT32_MAX;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    num1->value_type = s21_INFINITY;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    num1->value_type = s21_NAN;
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    // !test 13 trash * value
    initDecimal(num1);
    initDecimal(num2);
    num1->value_type = s21_INFINITY;
    num1->bits[SCALE] = UINT32_MAX;

    s21_from_float_to_decimal(5.5, num2);
    *result = s21_mul(*num1, *num2);
    ck_assert_int_eq(result->value_type, s21_NAN);

    FREE_VAR(num1);
    FREE_VAR(num2);
    FREE_VAR(result);
}
END_TEST

// jdreama
START_TEST(s21_mul_normal_int_tests) {
    s21_decimal num1;
    s21_decimal num2;
    s21_decimal mult;
    s21_decimal adds;
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    initDecimal(&adds);
    sign_t sign;
    // int scale1 = 2;
    // int scale2 = 2;
    unsigned mult_int = 0;
    num1.bits[0] = 0;
    num2.bits[0] = 0;
    set_sign(&num1, PLUS);
    set_sign(&num2, PLUS);
    // int scale1 = get_scale(num1);
    // int scale2 = get_scale(num2);

    for (int i = 0; i < 100; i++) {
        mult = s21_mul(num1, num2);
        mult_int = num2.bits[LOW] * num1.bits[LOW];

        ck_assert_msg(mult.bits[LOW] == mult_int, "false on i = %d\n", i);

        adds.bits[LOW] = i * 3 + (i + 2);
        num1 = s21_add(num1, adds);
        adds.bits[LOW] = i + (i + 3);
        num2 = s21_add(num2, adds);

        if (sign)
            sign = 0;
        else
            sign = 1;
        set_sign(&num1, sign);
        set_sign(&num2, sign);

        // // set scale
        // set_scale(&num1, scale1);
        // set_scale(&num2, scale2);
    }
}
END_TEST

START_TEST(s21_mul_normal_int_scale_tests) {
    s21_decimal num1;
    s21_decimal num2;
    s21_decimal mult;
    s21_decimal adds;
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    initDecimal(&adds);
    sign_t sign;
    int mult_int = 0;
    num1.bits[0] = 0;
    num2.bits[0] = 0;
    set_sign(&num1, PLUS);
    set_sign(&num2, PLUS);

    for (int i = 0; i < 13; i++) {
        alignScales(&num1, &num2);

        mult = s21_mul(num1, num2);
        mult_int = num2.bits[0] * num1.bits[0];

        // printDecimalBits(num1, HUMAN_FORMAT);
        // printDecimalBits(num2, HUMAN_FORMAT);
        // printDecimalBits(mult, HUMAN_FORMAT);
        // printf("%d\n", mult_int);
        ck_assert_int_eq(mult.bits[LOW], mult_int);

        adds.bits[LOW] = (i + 2);
        num1 = s21_add(num1, adds);
        adds.bits[LOW] = (i + 1);
        num2 = s21_add(num2, adds);

        if (sign)
            sign = 0;
        else
            sign = 1;
        set_sign(&num1, sign);
        set_sign(&num2, sign);

        // set scale
        set_scale(&num1, i / 3);
        set_scale(&num2, i / 3);
    }
}
END_TEST

START_TEST(s21_mul_handle_tests) {
    s21_decimal num1;
    s21_decimal num2;
    s21_decimal mult;
    s21_decimal result;
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    initDecimal(&result);
    // sign_t sign;
    set_sign(&num1, PLUS);
    set_sign(&num2, PLUS);
    // int scale;

    // New test
    num1.bits[2] = 0b00000000000000000000000000000000;
    num1.bits[1] = 0b00000000000000000000000000000000;
    num1.bits[0] = 0b00000000000000011110001001000000;

    num2.bits[2] = 0b00000000000000000000000000000000;
    num2.bits[1] = 0b00000000000000000000000000000000;
    num2.bits[0] = 0b00000000000000000000000000011101;

    result.bits[2] = 0b00000000000000000000000000000000;
    result.bits[1] = 0b00000000000000000000000000000000;
    result.bits[0] = 0b00000000001101101010000101000000;

    mult = s21_mul(num1, num2);

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(num2, HUMAN_FORMAT);
    // printDecimalBits(mult, HUMAN_FORMAT);
    // printDecimalBits(result, HUMAN_FORMAT);
    // printf("%d\n", mult.bits[LOW]);
    ck_assert_int_eq(s21_is_equal(mult, result), 0);

    // New test

    num2.bits[2] = 0b00000000000000000000000000000000;
    num2.bits[1] = 0b00000000000000000000000000000000;
    num2.bits[0] = 0b00000000000000000000000011101111;

    result.bits[2] = 0b00000000000000000000000000000000;
    result.bits[1] = 0b00000000000000000000000000000000;
    result.bits[0] = 0b00000001110000100011100111000000;

    mult = s21_mul(num1, num2);

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(num2, HUMAN_FORMAT);
    // printDecimalBits(mult, HUMAN_FORMAT);
    // printDecimalBits(result, HUMAN_FORMAT);
    // printf("%d\n", mult.bits[LOW]);
    ck_assert_int_eq(s21_is_equal(mult, result), 0);

    // New test

    num2.bits[2] = 0b00000000000000000000000000000000;
    num2.bits[1] = 0b00000000000000000000000000000000;
    num2.bits[0] = 0b00000000000000001110100011101111;

    result.bits[2] = 0b00000000000000000000000000000000;
    result.bits[1] = 0b00000000000000000000000000000001;
    result.bits[0] = 0b10110110110011000011100111000000;

    mult = s21_mul(num1, num2);

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(num2, HUMAN_FORMAT);
    // printDecimalBits(mult, HUMAN_FORMAT);
    // printDecimalBits(result, HUMAN_FORMAT);
    // printf("%d\n", mult.bits[LOW]);
    ck_assert_int_eq(s21_is_equal(mult, result), 0);

    // New test
    num1.bits[2] = 0b00000000000000000000000000000000;
    num1.bits[1] = 0b00000000000000000000000000000000;
    num1.bits[0] = 0b00000000000110011110001001000000;

    num2.bits[2] = 0b00000000000000000000000000000000;
    num2.bits[1] = 0b00000000000000000000000000000000;
    num2.bits[0] = 0b00000000000001011110100011101111;

    result.bits[2] = 0b00000000000000000000000000000000;
    result.bits[1] = 0b00000000000000000000000010011000;
    result.bits[0] = 0b11111000011101000011100111000000;

    mult = s21_mul(num1, num2);

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(num2, HUMAN_FORMAT);
    // printDecimalBits(mult, HUMAN_FORMAT);
    // printDecimalBits(result, HUMAN_FORMAT);
    // printf("%d\n", mult.bits[LOW]);
    ck_assert_int_eq(s21_is_equal(mult, result), 0);

    // New test
    num1.bits[2] = 0b00000000000000000000000000000000;
    num1.bits[1] = 0b00000000000000000000000000000100;
    num1.bits[0] = 0b00110110011001100010010000001101;

    num2.bits[2] = 0b00000000000000000000000000000000;
    num2.bits[1] = 0b00000000000000000000000000000111;
    num2.bits[0] = 0b11101111101101101011001110110101;

    result.bits[2] = 0b00000000000000000000000000100001;
    result.bits[1] = 0b01101110100101011111100110100100;
    result.bits[0] = 0b10100101101010101001010000110001;

    mult = s21_mul(num1, num2);

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(num2, HUMAN_FORMAT);
    // printDecimalBits(mult, HUMAN_FORMAT);
    // printDecimalBits(result, HUMAN_FORMAT);
    // printf("%d\n", mult.bits[LOW]);
    ck_assert_int_eq(s21_is_equal(mult, result), 0);

    // add infinity test
}
END_TEST

START_TEST(s21_mul_inf_tests) {
    s21_decimal num1;
    s21_decimal num2;
    s21_decimal mult;
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    // int scale;

    // TEST 1

    num1.value_type = s21_INFINITY;

    mult = s21_mul(num1, num2);

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(num2, HUMAN_FORMAT);
    // printDecimalBits(mult, HUMAN_FORMAT);
    // printDecimalBits(result, HUMAN_FORMAT);
    // printf("%d\n", mult.bits[LOW]);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 2
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);

    num1.value_type = s21_NEGATIVE_INFINITY;

    mult = s21_mul(num1, num2);

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(num2, HUMAN_FORMAT);
    // printDecimalBits(mult, HUMAN_FORMAT);
    // printDecimalBits(result, HUMAN_FORMAT);
    // printf("%d\n", mult.bits[LOW]);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 3
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    num1.value_type = 0;

    num2.value_type = s21_INFINITY;
    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 4
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    num1.value_type = 0;

    num2.value_type = s21_NEGATIVE_INFINITY;
    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 5

    num1.value_type = 0;

    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);

    num1.bits[2] = 0b00000000000000000000000000000000;
    num1.bits[1] = 0b00000000000000000000000000000100;
    num1.bits[0] = 0b00110110011001100010010000001101;
    set_sign(&num1, MINUS);
    num2.value_type = s21_NEGATIVE_INFINITY;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_INFINITY);

    // TEST 6
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    num2.bits[0] = 0b00110110011001100010010000001101;
    set_sign(&num2, MINUS);
    set_scale(&num2, 4);
    num2.value_type = 0;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_INFINITY);

    // TEST 7
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    set_sign(&num1, MINUS);
    set_scale(&num1, 3);
    num1.value_type = 0;
    num1.bits[0] = 0b00110110011001100010010000001101;

    set_sign(&num2, PLUS);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NEGATIVE_INFINITY);

    // TEST 8
    set_sign(&num1, 0);
    set_scale(&num1, 3);
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = 0;
    num2.bits[0] = 0b00110110011001100010010000001101;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NEGATIVE_INFINITY);

    // TEST 9
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    num1.value_type = s21_NEGATIVE_INFINITY;

    num2.value_type = s21_INFINITY;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 10
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    num1.value_type = s21_NEGATIVE_INFINITY;
    num2.value_type = s21_INFINITY;
    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 11
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);

    num1.value_type = s21_INFINITY;
    num2.value_type = s21_INFINITY;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_INFINITY);

    // TEST 12
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);

    num1.value_type = s21_NEGATIVE_INFINITY;

    num2.value_type = s21_NEGATIVE_INFINITY;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_INFINITY);

    // TEST 13
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    set_sign(&num1, MINUS);
    set_scale(&num1, 3);
    num1.value_type = s21_NAN;

    set_sign(&num2, PLUS);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 14
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_NAN;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);
}
END_TEST

START_TEST(s21_decimal_to_int_test) {
    s21_decimal num1;
    initDecimal(&num1);
    s21_decimal temp;
    initDecimal(&temp);
    int intag[10] = {555, -110, 0, -0, 35, 9999999, 1234567890, 1, -98, 001};
    int *pinta;
    int decResult = 0;
    int intResult = 0;
    for (int i = 0; i < 10; i++) {
        pinta = &intag[i];
        s21_from_int_to_decimal(intag[i], &num1);
        // printf("debag =  %d\n", intag[i]);
        s21_from_decimal_to_int(num1, pinta);
        // printf("debag =  %d\n", intag[i]);
        decResult = num1.bits[LOW];
        if (get_sign(num1)) decResult *= -1;
        ck_assert_int_eq(decResult, *pinta);
        initDecimal(&num1);
    }

    // +scale test
    num1.bits[2] = 0b00000000000000000000000000000000;
    num1.bits[1] = 0b00000000000000000000000000000000;
    num1.bits[0] = 0b00110110011001100010010000001101;
    num1.bits[LOW] = 12345678;
    set_scale(&num1, 27);

    s21_from_decimal_to_int(num1, &intResult);
    temp = num1;
    int scale = get_scale(temp);
    if (scale) scale_down(&temp, scale, 1);
    decResult = temp.bits[LOW];
    // printf("debag =  %d\n", intResult);
    ck_assert_int_eq(decResult, intResult);
}
END_TEST

START_TEST(s21_decimal_to_int_error_test) {
    s21_decimal num1;
    initDecimal(&num1);
    int *pinta;
    int decResult = 1;
    int error = 0;
    pinta = &error;

    // New error test
    num1.bits[2] = 0b00000000000000000000000000000000;
    num1.bits[1] = 0b00000000000000000000000000000100;
    num1.bits[0] = 0b00110110011001100010010000001101;
    error = s21_from_decimal_to_int(num1, pinta);
    // printf("debag =  %d\n", num1.bits[MID]);
    ck_assert_int_eq(decResult, *pinta);

    // New error test
    num1.bits[2] = 0b00000000000000000000000000000001;
    num1.bits[1] = 0b00000000000000000000000000000000;
    num1.bits[0] = 0b00110110011001100010010000001101;
    error = s21_from_decimal_to_int(num1, pinta);
    // printf("debag =  %d\n", );
    ck_assert_int_eq(decResult, *pinta);

    // New error test
    num1.bits[2] = 0b10000000000000000000000000000000;
    num1.bits[1] = 0b00000000000000000000000000000000;
    num1.bits[0] = 0b00000000000000000000000000000000;
    error = s21_from_decimal_to_int(num1, pinta);
    // printf("debag =  %d\n", );
    ck_assert_int_eq(decResult, *pinta);
}
END_TEST

START_TEST(s21_nigate_test) {
    s21_decimal num1;
    initDecimal(&num1);
    s21_decimal temp;
    initDecimal(&temp);
    int intag[10] = {555, -110, 0, -0, 35, 9999999, 1234567890, 1, -98, 001};
    int decSign = 0;
    int intSign = 0;
    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag[i], &num1);
        if (get_sign(num1))
            intSign = 0;
        else
            intSign = 1;
        // printDecimalBits(num1, HUMAN_FORMAT);
        temp = s21_negate(num1);
        // printDecimalBits(temp, HUMAN_FORMAT);
        // printf("result sign = %d\n", intSign);
        decSign = get_sign(temp);
        ck_assert_int_eq(decSign, intSign);
        initDecimal(&temp);
        initDecimal(&num1);
    }
}
END_TEST

START_TEST(s21_truncate_test) {
    s21_decimal num1;
    initDecimal(&num1);
    s21_decimal num2;
    initDecimal(&num2);
    s21_decimal temp;
    initDecimal(&temp);
    int intag[10] = {55523, -110, 0, -0, 35, 9999999, 1234567890, 9, -980, 1};
    int result[10] = {555, -1, 0, -0, 0, 99999, 12345678, 0, -9, 0};
    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag[i], &num2);
        set_scale(&num2, 2);
        s21_from_int_to_decimal(result[i], &temp);
        if (result[i] < 0) set_sign(&temp, MINUS);

        num1 = s21_truncate(num2);

        // printf("result[%d] = %d, dec = %d\n", i, result[i], num1.bits[LOW]);
        // printf("chislo =\n");
        // printDecimalBits(num2, HUMAN_FORMAT);
        // printf("tranc =\n");
        // printDecimalBits(num1, HUMAN_FORMAT);
        // printf("awating =\n");
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_msg(s21_is_equal(num1, temp) == 0, "test i = %d\n", i);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }

    // TEST 2
    int intag2[10] = {5552300,  -110010101, 0,        -31231120, 35,
                      99999999, 1234567890, 90000000, -98,       10001};
    int result2[10] = {55, -1100, 0, -312, 0, 999, 12345, 900, -0, 0};

    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag2[i], &num2);
        set_scale(&num2, 5);
        s21_from_int_to_decimal(result2[i], &temp);
        if (i == 8) set_sign(&temp, MINUS);
        num1 = s21_truncate(num2);

        // printf("result[%d] = %d, dec = %d\n", i, result2[i], num1.bits[LOW]);
        // printDecimalBits(num1, HUMAN_FORMAT);
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_msg(s21_is_equal(num1, temp) == 0, "failed test i= %d\n", i);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }

    // TEST 3
    set_scale(&num2, 1);
    num2.bits[2] = 0b00000000000000000000000000000001;
    num2.bits[1] = 0b00000000000000000000000000000100;
    num2.bits[0] = 0b00110110011001100010010010101000;

    // 1 00000000000000000000000000000100 00110110011001100010010010101000
    num1 = s21_truncate(num2);

    temp.bits[2] = 0b00000000000000000000000000000000;
    temp.bits[1] = 0b00011001100110011001100110011010;
    temp.bits[0] = 0b00000101011100001001110101000100;
    // tttt 11001100110011001100110011010 00000101011100001001110101000100

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(temp, HUMAN_FORMAT);
    ck_assert_int_eq(s21_is_equal(num1, temp), 0);
    initDecimal(&temp);
    initDecimal(&num1);
    initDecimal(&num2);
}
END_TEST

START_TEST(s21_round_test) {
    s21_decimal num1;
    initDecimal(&num1);
    s21_decimal num2;
    initDecimal(&num2);
    s21_decimal temp;
    initDecimal(&temp);
    int intag[10] = {55529,   -11055,     11045, -0,  35,
                     9999999, 1234567890, 9,     -98, 001};
    int result[10] = {555, -111, 110, -0, 0, 100000, 12345679, 0, -1, 0};
    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag[i], &num2);
        set_scale(&num2, 2);
        s21_from_int_to_decimal(result[i], &temp);

        num1 = s21_round(num2);

        // printf("result[%d] = %d, dec = %d\n", i, result[i], num1.bits[LOW]);
        // printDecimalBits(num1, HUMAN_FORMAT);
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_int_eq(s21_is_equal(num1, temp), 0);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }

    // TEST 2
    int intag2[10] = {5552300,  -110010101, 0,        -31231120, 35,
                      99999999, 1234567890, 90000000, -98,       10001};
    int result2[10] = {56, -1100, 0, -312, 0, 1000, 12346, 900, -0, 0};

    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag2[i], &num2);
        set_scale(&num2, 5);
        s21_from_int_to_decimal(result2[i], &temp);
        if (i == 8) set_sign(&temp, MINUS);

        num1 = s21_round(num2);

        // printf("intag [%d] = %d, result[%d] = %d, dec = %d\n", i, intag2[i],
        // i,
        //        result2[i], num1.bits[LOW]);
        // printDecimalBits(num1, HUMAN_FORMAT);
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_msg(s21_is_equal(num1, temp) == 0, "failed test i= %d\n", i);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }

    // TEST 3
    set_scale(&num2, 1);
    num2.bits[2] = 0b00000000000000000000000000000001;
    num2.bits[1] = 0b00000000000000000000000000000100;
    num2.bits[0] = 0b00110110011001100010010010101000;

    // 1 00000000000000000000000000000100 00110110011001100010010010101000
    num1 = s21_round(num2);

    temp.bits[2] = 0b00000000000000000000000000000000;
    temp.bits[1] = 0b00011001100110011001100110011010;
    temp.bits[0] = 0b00000101011100001001110101000100;
    // tttt 11001100110011001100110011010 00000101011100001001110101000100

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(temp, HUMAN_FORMAT);
    ck_assert_int_eq(s21_is_equal(num1, temp), 0);
    initDecimal(&temp);
    initDecimal(&num1);
    initDecimal(&num2);
}
END_TEST

START_TEST(s21_floor_test) {
    s21_decimal num1;
    initDecimal(&num1);
    s21_decimal num2;
    initDecimal(&num2);
    s21_decimal temp;
    initDecimal(&temp);
    int intag[10] = {55599,   -11055,     11045, -0,  35,
                     9999999, 1234567890, 9,     -98, 001};
    int result[10] = {555, -111, 110, -0, 0, 99999, 12345678, 0, -1, 0};
    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag[i], &num2);
        set_scale(&num2, 2);
        s21_from_int_to_decimal(result[i], &temp);

        num1 = s21_floor(num2);

        // printf("result[%d] = %d, dec = %d\n", i, result[i], num1.bits[LOW]);
        // printDecimalBits(num1, HUMAN_FORMAT);
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_int_eq(s21_is_equal(num1, temp), 0);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }

    // TEST 2
    int intag2[10] = {5552300,  -110010101, 0,        -31231120, 35,
                      99999999, 1234567890, 90000000, -98,       10001};
    int result2[10] = {55, -1101, 0, -313, 0, 999, 12345, 900, -1, 0};

    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag2[i], &num2);
        set_scale(&num2, 5);
        s21_from_int_to_decimal(result2[i], &temp);

        num1 = s21_floor(num2);

        // printf("result[%d] = %d, dec = %d\n", i, result2[i], num1.bits[LOW]);
        // printDecimalBits(num1, HUMAN_FORMAT);
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_int_eq(s21_is_equal(num1, temp), 0);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }

    // TEST 3
    set_scale(&num2, 1);
    set_sign(&num2, MINUS);
    num2.bits[2] = 0b00000000000000000000000000000001;
    num2.bits[1] = 0b00000000000000000000000000000100;
    num2.bits[0] = 0b00110110011001100010010010101000;

    // 1 00000000000000000000000000000100 00110110011001100010010010101000
    num1 = s21_floor(num2);

    set_sign(&temp, MINUS);
    temp.bits[2] = 0b00000000000000000000000000000000;
    temp.bits[1] = 0b00011001100110011001100110011010;
    temp.bits[0] = 0b00000101011100001001110101000101;
    // tttt 11001100110011001100110011010 00000101011100001001110101000100

    // printDecimalBits(num1, HUMAN_FORMAT);
    // printDecimalBits(temp, HUMAN_FORMAT);
    ck_assert_int_eq(s21_is_equal(num1, temp), 0);
    initDecimal(&temp);
    initDecimal(&num1);
    initDecimal(&num2);
}
END_TEST

START_TEST(is_greater_or_equal) {
    s21_decimal num1;
    initDecimal(&num1);
    s21_decimal num2;
    initDecimal(&num2);
    s21_decimal temp;
    initDecimal(&temp);
    int intag[10] = {55599,   111055,     11045, -0,  35,
                     9999999, 1234567890, 9,     -98, 13};
    int result[10] = {555, -199911, 110, -0, 0, 99999, 12345678, 0, -100, 2};
    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag[i], &num2);
        set_scale(&num2, 0);
        s21_from_int_to_decimal(result[i], &temp);

        // printf("result[%d] = %d, num2 = %d\n", i, temp.bits[LOW],
        //        num2.bits[LOW]);
        // printDecimalBits(num1, HUMAN_FORMAT);
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_int_eq(s21_is_greater_or_equal(num2, temp), 0);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }

    // TEST 2
    int intag2[10] = {555000, -110010101, 0,         -31231120, 35,
                      899123, -12345000,  -90000000, -98,       -10001};
    int result2[10] = {555, -1101, 36, -313, 35, 999, 12345, 900, 0, 0};

    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag2[i], &num2);
        set_scale(&num2, 3);
        s21_from_int_to_decimal(result2[i], &temp);

        // printf("result[%d] = %d, num2 = %d\n", i, temp.bits[LOW],
        //        num2.bits[LOW]);
        // printDecimalBits(num2, HUMAN_FORMAT);
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_int_eq(s21_is_greater_or_equal(temp, num2), 0);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }
    // TEST 3
    int intag3[10] = {556100, -110010101, 0,         -31231120, 35,
                      899123, -12345000,  -90000000, -98,       -10001};
    int result3[10] = {556, -1101, 36, -313, 35, 999, 12345, 900, 0, 0};

    for (int i = 0; i < 10; i++) {
        s21_from_int_to_decimal(intag3[i], &num2);
        set_scale(&num2, 3);
        s21_from_int_to_decimal(result3[i], &temp);

        // printf("result[%d] = %d, num2 = %d\n", i, temp.bits[LOW],
        //        num2.bits[LOW]);
        // printDecimalBits(num2, HUMAN_FORMAT);
        // printDecimalBits(temp, HUMAN_FORMAT);
        ck_assert_int_eq(s21_is_not_equal(temp, num2), 0);
        initDecimal(&temp);
        initDecimal(&num1);
        initDecimal(&num2);
    }
}

END_TEST

START_TEST(s21_add_inf_tests) {
    s21_decimal num1;
    s21_decimal num2;
    s21_decimal mult;
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    // int scale;

    // TEST 9
    set_sign(&num1, MINUS);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    num2.bits[2] = 0b00000000000000000000000000000000;
    num2.bits[1] = 0b00000000000000000000100000000000;
    num2.bits[0] = 0b00100000000000000000000000000000;
    set_sign(&num2, PLUS);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_add(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 10

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_add(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 11

    set_sign(&num1, MINUS);
    set_scale(&num1, 3);
    num1.value_type = s21_INFINITY;

    set_sign(&num2, PLUS);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_add(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_INFINITY);

    // TEST 12

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_NEGATIVE_INFINITY;

    mult = s21_add(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NEGATIVE_INFINITY);

    // TEST 13

    set_sign(&num1, MINUS);
    set_scale(&num1, 3);
    num1.value_type = s21_NAN;

    set_sign(&num2, PLUS);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_add(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 14

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_NAN;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);
}
END_TEST

START_TEST(s21_sub_inf_tests) {
    s21_decimal num1;
    s21_decimal num2;
    s21_decimal mult;
    initDecimal(&num1);
    initDecimal(&num2);
    initDecimal(&mult);
    // int scale;

    // TEST 9
    set_sign(&num1, MINUS);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, PLUS);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_sub(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 10

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_NEGATIVE_INFINITY;

    mult = s21_sub(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 11

    set_sign(&num1, MINUS);
    set_scale(&num1, 3);
    num1.value_type = s21_INFINITY;

    set_sign(&num2, PLUS);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_sub(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 12

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NORMAL_VALUE;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_NEGATIVE_INFINITY;

    mult = s21_sub(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_INFINITY);

    // TEST 12-2

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NORMAL_VALUE;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_sub(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NEGATIVE_INFINITY);

    // TEST 12-3

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_NORMAL_VALUE;

    mult = s21_sub(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_INFINITY);

    // TEST 12-4

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_NORMAL_VALUE;

    mult = s21_sub(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NEGATIVE_INFINITY);

    // TEST 13

    set_sign(&num1, MINUS);
    set_scale(&num1, 3);
    num1.value_type = s21_NAN;

    set_sign(&num2, PLUS);
    set_scale(&num2, 4);
    num2.value_type = s21_INFINITY;

    mult = s21_sub(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // TEST 14

    set_sign(&num1, 0);
    set_scale(&num1, 3);
    num1.value_type = s21_NEGATIVE_INFINITY;

    set_sign(&num2, 0);
    set_scale(&num2, 4);
    num2.value_type = s21_NAN;

    mult = s21_mul(num1, num2);
    ck_assert_int_eq(mult.value_type, s21_NAN);

    // camaerie test
    s21_decimal maxNegativeVal;
    initDecimal(&maxNegativeVal);
    maxNegativeVal.bits[LOW] = UINT32_MAX;
    maxNegativeVal.bits[MID] = UINT32_MAX;
    maxNegativeVal.bits[HIGH] = UINT32_MAX;
    set_sign(&maxNegativeVal, MINUS);

    s21_decimal ten;
    initDecimal(&ten);
    ten.bits[LOW] = 10;

    ck_assert_msg(
        s21_sub(maxNegativeVal, ten).value_type == s21_NEGATIVE_INFINITY,
        "MAX NEGATIVE VAL - 10 != NEGATIVE INF");
}
END_TEST

// end jdreama END JDREAMA

// camaerie
START_TEST(mod_base_test) {
    // int debugNumI = 1;
    // int debugNumJ = 0;
    for (int i = -10; i <= 15; i++) {  // change back to 100 (!)
        // if (i == debugNumI) printf("i:%d\n", i);
        for (int j = 10; j >= -10; j--) {
            if (j != 0) {
                // if (i == debugNumI) printf("j: %d\n", j);
                // if (i == debugNumI || j == debugNumJ) printf("here 1
                // \n");
                s21_decimal dec_i;
                initDecimal(&dec_i);
                set_sign(&dec_i, i > 0 ? PLUS : MINUS);
                dec_i.bits[LOW] = abs(i);
                s21_decimal dec_j;
                initDecimal(&dec_j);
                set_sign(&dec_j, j > 0 ? PLUS : MINUS);
                dec_j.bits[LOW] = abs(j);

                s21_decimal result = s21_mod(dec_i, dec_j);
                // if (i == debugNumI || j == debugNumJ) printf("here 2
                // \n");
                // int testSign = -2;
                // if (i % j > 0) testSign = PLUS;

                // if (i % j < 0) testSign = MINUS;

                int testResult = abs(i) % abs(j);
                // if (i == debugNumI || j == debugNumJ) printf("here 3
                // \n");
                ck_assert_msg(result.bits[LOW] == abs(testResult),
                              "test failed on i: %d, j: %d, bits[LOW]: %u - "
                              "different numbers",
                              i, j, result.bits[LOW]);
                // if (testSign != -2)
                //     ck_assert_msg(get_sign(result) == abs(testSign),
                //                   "test failed on i: %d, j: %d, bits[LOW]: %u
                //                   "
                //                   "- different SIGN. %% sign: %d, your: %d",
                //                   i, j, result.bits[LOW], testSign,
                //                   get_sign(result));
            }
        }
    }
    s21_decimal *num1 = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal *num2 = (s21_decimal *)malloc(sizeof(s21_decimal));
    s21_decimal num3;
    initDecimal(num1);
    initDecimal(num2);

    num1->bits[LOW] = 0b01110000000000000000000000000000;
    num1->bits[MID] = 0b10110011000000110001000010100111;
    num1->bits[HIGH] = 0b11100010001011101010010010010011;

    num2->bits[LOW] = 1;
    set_scale(num2, 3);

    num3 = s21_mod(*num1, *num2);
    initDecimal(num2);
    // printDecimalBits(num3, HUMAN_FORMAT);
    // printf("value1 = %d\n", num3.value_type);
    // printDecimalBits(*num2, HUMAN_FORMAT);
    // printf("value2 = %d\n", num2->value_type);
    int equal = num3.bits[LOW] + num3.bits[MID] + num3.bits[HIGH];
    // printf("equal = %d\n", equal);
    ck_assert_msg(equal == 0);

    FREE_VAR(num1);
    FREE_VAR(num2);
}
END_TEST

START_TEST(mod_infAndSign_tests) {
    s21_decimal num1;
    s21_decimal num2;
    initDecimal(&num1);
    initDecimal(&num2);

    num1.bits[LOW] = 5;
    num2.bits[LOW] = 3;

    set_sign(&num1, PLUS);
    set_sign(&num2, PLUS);
    ck_assert(get_sign(s21_mod(num1, num2)) == PLUS);

    set_sign(&num1, MINUS);
    set_sign(&num2, PLUS);
    ck_assert(get_sign(s21_mod(num1, num2)) == MINUS);

    set_sign(&num1, PLUS);
    set_sign(&num2, MINUS);
    ck_assert(get_sign(s21_mod(num1, num2)) == PLUS);

    set_sign(&num1, MINUS);
    set_sign(&num2, MINUS);
    ck_assert(get_sign(s21_mod(num1, num2)) == MINUS);

    initDecimal(&num1);
    initDecimal(&num2);  // zero
    num1.bits[LOW] = 5;
    ck_assert(s21_mod(num2, num2).value_type == s21_NAN);
}
END_TEST

START_TEST(rounding_test_1) {
    s21_decimal num1;
    initDecimal(&num1);
    s21_decimal num2;
    initDecimal(&num2);

    num1.bits[LOW] = __UINT32_MAX__;
    num1.bits[MID] = __UINT32_MAX__;
    num1.bits[HIGH] = __UINT32_MAX__;

    set_sign(&num1, PLUS);

    num2.bits[LOW] = 6;
    set_sign(&num2, PLUS);
    set_scale(&num2, 1);

    s21_decimal result = s21_sub(num1, num2);

    s21_decimal expected;
    initDecimal(&expected);

    expected.bits[LOW] = __UINT32_MAX__ - 1;
    expected.bits[MID] = __UINT32_MAX__;
    expected.bits[HIGH] = __UINT32_MAX__;

    set_sign(&expected, PLUS);

    ck_assert(s21_is_equal(result, expected) == 0);
}
END_TEST

START_TEST(s21_div_tests_1) {
    s21_decimal num1;
    initDecimal(&num1);
    num1.bits[LOW] = 0xA;
    num1.bits[MID] = 0x0;
    num1.bits[HIGH] = 0x0;
    num1.bits[3] = 0x80000000;

    s21_decimal num2;
    initDecimal(&num2);
    num2.bits[LOW] = 0xA;
    num2.bits[MID] = 0x0;
    num2.bits[HIGH] = 0x0;
    num2.bits[3] = 0x80000000;

    s21_decimal expected;
    initDecimal(&expected);
    expected.bits[LOW] = 1;
    expected.bits[MID] = 0;
    expected.bits[HIGH] = 0;
    expected.bits[3] = 0;

    ck_assert_msg(s21_is_equal(expected, s21_div(num1, num2)) == 0,
                  "division failed on 1");

    num1.bits[LOW] = 0xA;
    num1.bits[MID] = 0x0;
    num1.bits[HIGH] = 0x0;
    num1.bits[3] = 0x80000000;

    num2.bits[LOW] = 0x1;
    num2.bits[MID] = 0x0;
    num2.bits[HIGH] = 0x0;
    num2.bits[3] = 0x80170000;

    expected.bits[LOW] = 0xA1000000;
    expected.bits[MID] = 0x1BCECCED;
    expected.bits[HIGH] = 0xD3C2;
    expected.bits[3] = 0x0;

    ck_assert_msg(s21_is_equal(expected, s21_div(num1, num2)) == 0,
                  "division failed on 2");

    num1.bits[LOW] = 0xA;
    num1.bits[MID] = 0x0;
    num1.bits[HIGH] = 0x0;
    num1.bits[3] = 0x80000000;

    num2.bits[LOW] = 0x9;
    num2.bits[MID] = 0x0;
    num2.bits[HIGH] = 0x0;
    num2.bits[3] = 0x80000000;

    expected.bits[LOW] = 0x671C71C7;
    expected.bits[MID] = 0x450CAD4F;
    expected.bits[HIGH] = 0x23E6E54C;
    expected.bits[3] = 0x1C0000;

    ck_assert_msg(s21_is_equal(expected, s21_div(num1, num2)) == 0,
                  "division failed on 3");

    num1.bits[LOW] = 0x64;
    num1.bits[MID] = 0x0;
    num1.bits[HIGH] = 0x0;
    num1.bits[3] = 0x80000000;

    num2.bits[LOW] = 0x52;
    num2.bits[MID] = 0x0;
    num2.bits[HIGH] = 0x0;
    num2.bits[3] = 0x80180000;

    expected.bits[LOW] = 0x3F3831F4;
    expected.bits[MID] = 0xA971D0F3;
    expected.bits[HIGH] = 0x27679185;
    expected.bits[3] = 0x40000;

    ck_assert_msg(s21_is_equal(expected, s21_div(num1, num2)) == 0,
                  "division failed on 4");

    num1.bits[LOW] = 0x5E;
    num1.bits[MID] = 0x0;
    num1.bits[HIGH] = 0x0;
    num1.bits[3] = 0x80000000;

    num2.bits[LOW] = 0xB;
    num2.bits[MID] = 0x0;
    num2.bits[HIGH] = 0x0;
    num2.bits[3] = 0x80000000;

    expected.bits[LOW] = 0xED1745D1;
    expected.bits[MID] = 0x272447D9;
    expected.bits[HIGH] = 0x1B9CA263;
    expected.bits[3] = 0x1B0000;

    ck_assert_msg(s21_is_equal(expected, s21_div(num1, num2)) == 0,
                  "division failed on 5");

    s21_decimal ten;
    initDecimal(&ten);
    initDecimal(&num1);
    initDecimal(&num2);
    ten.bits[LOW] = 10;
    num1.bits[LOW] = 1;
    num2.bits[LOW] = 1;

    for (int i = 0; i < 27; i++) {
        num1 = s21_mul(num1, ten);
        num2 = s21_div(num2, ten);
    }
    s21_decimal res = s21_div(num1, num2);
    ck_assert_msg(res.bits[LOW] == 0 && res.bits[MID] == 0 &&
                      res.bits[HIGH] == 0 && res.value_type == s21_NAN,
                  "division overflow fail on 6");
}
END_TEST

// INFS
START_TEST(s21_div_tests_2) {
    s21_decimal INF_P;
    initDecimal(&INF_P);
    INF_P.value_type = s21_INFINITY;

    s21_decimal INF_M;
    initDecimal(&INF_M);
    INF_M.value_type = s21_NEGATIVE_INFINITY;

    s21_decimal expected;
    initDecimal(&expected);

    s21_decimal num_P;
    initDecimal(&num_P);
    num_P.bits[LOW] = 5;

    s21_decimal num_M;
    initDecimal(&num_M);
    num_M.bits[LOW] = 5;
    set_sign(&num_M, MINUS);

    s21_decimal zero;
    initDecimal(&zero);

    ck_assert_msg(s21_div(INF_P, INF_P).value_type == s21_NAN,
                  "division INF_P / INF_P != NaN");
    ck_assert_msg(s21_div(INF_M, INF_M).value_type == s21_NAN,
                  "division INF_M / INF_M != NaN");

    ck_assert_msg(s21_is_equal(s21_div(num_P, INF_P), zero) == 0,
                  "division num_P / INF_P != zero");
    ck_assert_msg(s21_is_equal(s21_div(num_P, INF_M), zero) == 0,
                  "division num_P / INF_M != zero");
    ck_assert_msg(s21_is_equal(s21_div(num_M, INF_P), zero) == 0,
                  "division num_M / INF_P != zero");
    ck_assert_msg(s21_is_equal(s21_div(num_M, INF_M), zero) == 0,
                  "division num_M / INF_M != zero");

    ck_assert_msg(s21_div(INF_P, num_P).value_type == s21_INFINITY,
                  "division INF_P / num_P != INF_P");
    ck_assert_msg(s21_div(INF_P, num_M).value_type == s21_NEGATIVE_INFINITY,
                  "division INF_P / num_M != INF_M");
    ck_assert_msg(s21_div(INF_M, num_P).value_type == s21_NEGATIVE_INFINITY,
                  "division INF_M / num_P != INF_M");
    ck_assert_msg(s21_div(INF_M, num_M).value_type == s21_INFINITY,
                  "division INF_M / num_M != INF_P");
}
END_TEST

// end camaerie

Suite *decimal_suite(void) {
    Suite *suite = suite_create("decimal_suite");
    TCase *cases = tcase_create("decimal case");

    tcase_add_test(cases, s21_from_float_to_decimal_normal_case);
    tcase_add_test(cases, s21_from_float_to_decimal_extra_input_case);
    tcase_add_test(cases, s21_from_decimal_to_float_normal_case);
    tcase_add_test(cases, s21_mul_extra_case);
    tcase_add_test(cases, s21_mul_invalid_value_case);

    // jdreama
    tcase_add_test(cases, s21_mul_normal_int_tests);
    tcase_add_test(cases, s21_mul_normal_int_scale_tests);
    tcase_add_test(cases, s21_mul_handle_tests);
    tcase_add_test(cases, s21_mul_inf_tests);
    tcase_add_test(cases, s21_add_inf_tests);
    tcase_add_test(cases, s21_sub_inf_tests);

    tcase_add_test(cases, s21_decimal_to_int_test);
    tcase_add_test(cases, s21_decimal_to_int_error_test);
    tcase_add_test(cases, s21_nigate_test);
    tcase_add_test(cases, s21_truncate_test);
    tcase_add_test(cases, s21_round_test);
    tcase_add_test(cases, s21_floor_test);
    tcase_add_test(cases, is_greater_or_equal);
    // tcase_add_test(cases, is_not_equal);

    // camaerie
    tcase_add_test(cases, mod_base_test);
    tcase_add_test(cases, mod_infAndSign_tests);
    tcase_add_test(cases, rounding_test_1);
    tcase_add_test(cases, s21_div_tests_1);
    tcase_add_test(cases, s21_div_tests_2);
    tcase_set_timeout(cases, 9999999);

    suite_add_tcase(suite, cases);
    return suite;
}
