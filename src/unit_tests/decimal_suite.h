#ifndef SRC_UNIT_TESTS_DECIMAL_SUITE_H_
#define SRC_UNIT_TESTS_DECIMAL_SUITE_H_

#include <check.h>
#include <math.h>

#include "s21_decimal.h"

Suite *decimal_suite(void);

#endif  // SRC_UNIT_TESTS_DECIMAL_SUITE_H_
