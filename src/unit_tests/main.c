#include <check.h>

#include "decimal_suite.h"
#include "external_tests.h"
#include "s21_decimal.h"
#include "utils_suite.h"

int main(void) {
    int number_failed;
    Suite *suite;
    SRunner *suite_runner;

    number_failed = 0;

    suite = utils_suite();
    suite_runner = srunner_create(suite);
    srunner_run_all(suite_runner, CK_NORMAL);
    number_failed = srunner_ntests_failed(suite_runner);
    srunner_free(suite_runner);

    suite = decimal_suite();
    suite_runner = srunner_create(suite);
    srunner_run_all(suite_runner, CK_NORMAL);
    number_failed += srunner_ntests_failed(suite_runner);
    srunner_free(suite_runner);

    Suite *s1 = AriphmeticFunctions();
    Suite *s3 = AddFunctions();
    Suite *s4 = ChangeFunctions();

    SRunner *sr1 = srunner_create(s1);
    SRunner *sr3 = srunner_create(s3);
    SRunner *sr4 = srunner_create(s4);

    srunner_run_all(sr1, CK_NORMAL);
    number_failed += srunner_ntests_failed(sr1);
    srunner_free(sr1);

    srunner_run_all(sr3, CK_NORMAL);
    number_failed += srunner_ntests_failed(sr3);
    srunner_free(sr3);
    srunner_run_all(sr4, CK_NORMAL);
    number_failed += srunner_ntests_failed(sr4);
    srunner_free(sr4);

    printf("Total number Failures: %d\n\n", number_failed);
    return 0;
}
