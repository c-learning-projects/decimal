#include "utils_suite.h"

#include <check.h>

#include "s21_decimal.h"

// TODO(camaerie): need to put this somewhere

int int_pow(int a, int b) {
    int res = 1;
    while (b) {
        res *= a;
        b--;
    }
    return res;
}

START_TEST(initDecimal_test) {
    s21_decimal number;
    initDecimal(&number);
    ck_assert_int_eq(number.bits[LOW] | number.bits[HIGH] | number.bits[MID] |
                         number.bits[SCALE],
                     0);

    // !SEGV or INVALID INPUT
    // initDecimal(NULL);
}
END_TEST

START_TEST(getBit_test) {
    s21_decimal number;

    initDecimal(&number);
    number.bits[LOW] |= 1 << 5U;
    ck_assert_int_eq(getBit(number, 32 * 0 + 5), 1);

    initDecimal(&number);
    number.bits[MID] |= 1 << 5U;
    ck_assert_int_eq(getBit(number, 32 * 1 + 5), 1);

    initDecimal(&number);
    number.bits[HIGH] |= 1 << 5U;
    ck_assert_int_eq(getBit(number, 32 * 2 + 5), 1);

    initDecimal(&number);
    number.bits[SCALE] |= 1 << 31U;
    ck_assert_int_eq(getBit(number, SIGN_POSITION), 1);

    initDecimal(&number);
    number.bits[SCALE] |= 1 << 16U;
    ck_assert_int_eq(getBit(number, SCALE_OFFSET), 1);

    // !SEGV or INVALID INPUT
    // initDecimal(&number);
    // number.bits[SCALE] |= 23 << 23U;
    // ck_assert_int_eq(getBit(number, SCALE_OFFSET + 8), 1);

    // initDecimal(&number);
    // ck_assert_int_eq(getBit(number, SCALE_OFFSET - 1), -1);
    // ck_assert_int_eq(getBit(number, SCALE_OFFSET + 9), -1);
    // ck_assert_int_eq(getBit(number, SIGN_POSITION - 1), -1);
    // ck_assert_int_eq(getBit(number, 2048), -1);
    // ck_assert_int_eq(getBit(number, -2048), -1);
}
END_TEST

START_TEST(setBit_test) {
    s21_decimal number;

    initDecimal(&number);
    setBit(&number, 0, 1);
    ck_assert_int_eq(getBit(number, 0), 1);

    initDecimal(&number);
    setBit(&number, 33, 1);
    ck_assert_int_eq(getBit(number, 33), 1);

    initDecimal(&number);
    setBit(&number, 70, 1);
    ck_assert_int_eq(getBit(number, 70), 1);

    initDecimal(&number);
    setBit(&number, SCALE_OFFSET + 5, 1);
    ck_assert_int_eq(getBit(number, SCALE_OFFSET + 5), 1);

    initDecimal(&number);
    number.bits[LOW] = 0xFFFFFFFF;
    setBit(&number, 0, 0);
    ck_assert_int_eq(getBit(number, 0), 0);
    ck_assert_int_eq(getBit(number, 1), 1);
    ck_assert_int_eq(getBit(number, 31), 1);
    ck_assert_int_eq(getBit(number, 32), 0);

    // !SEGV or INVALID INPUT
    // initDecimal(&number);
    // ck_assert_int_eq(setBit(&number, SCALE_OFFSET - 1, 1), -1);
    // ck_assert_int_eq(setBit(&number, SCALE_OFFSET + 8, 1), -1);
    // ck_assert_int_eq(getBit(number, SIGN_POSITION - 1), -1);

    // initDecimal(&number);
    // ck_assert_int_eq(setBit(&number, 2048, 0), -1);
    // ck_assert_int_eq(setBit(&number, -2048, 0), -1);
}
END_TEST

START_TEST(get_scale_test) {
    s21_decimal number;

    initDecimal(&number);
    ck_assert_int_eq(get_scale(number), 0);

    initDecimal(&number);
    number.bits[SCALE] = 0x00FF0000;
    ck_assert_int_eq(get_scale(number), 255);

    initDecimal(&number);
    number.bits[SCALE] = 0xFF00FFFF;
    ck_assert_int_eq(get_scale(number), 0);
}
END_TEST

START_TEST(set_scale_test) {
    s21_decimal number;

    initDecimal(&number);
    ck_assert_int_eq(get_scale(number), 0);

    initDecimal(&number);
    number.bits[SCALE] = 0xFFFFFFFF;
    set_scale(&number, 0);
    ck_assert_int_eq(get_scale(number), 0);

    initDecimal(&number);
    set_scale(&number, 5);
    ck_assert_int_eq(get_scale(number), 5);

    // !SEGV or INVALID INPUT
    // initDecimal(&number);
    // set_scale(&number, -5);
    // ck_assert_int_eq(get_scale(number), 0);

    // ck_assert_int_eq(set_scale(NULL, NULL), -1);

    // initDecimal(&number);
    // set_scale(&number, -500000);
    // ck_assert_int_eq(get_scale(number), 0);

    // initDecimal(&number);
    // set_scale(&number, -500000);
    // ck_assert_int_eq(get_scale(number), 0);

    // initDecimal(&number);
    // set_scale(&number, 50000);
    // ck_assert_int_eq(get_scale(number), 0);
}
END_TEST

START_TEST(shiftRight_test) {
    s21_decimal number;

    initDecimal(&number);
    number.bits[LOW] = 1 << 31;
    shiftRight(&number);
    ck_assert_int_eq(number.bits[LOW], 1U << 30);

    initDecimal(&number);
    number.bits[LOW] = 1;
    shiftRight(&number);
    ck_assert_int_eq(number.bits[MID], 0);

    initDecimal(&number);
    number.bits[MID] = 1;
    shiftRight(&number);
    ck_assert_int_eq(number.bits[LOW], 1U << 31);

    initDecimal(&number);
    number.bits[HIGH] = 1;
    shiftRight(&number);
    shiftRight(&number);
    ck_assert_int_eq(number.bits[MID], 1U << 30);

    // !SEGV or INVALID INPUT
    // ck_assert_int_eq(shiftRight(NULL), -1);
}
END_TEST

START_TEST(shiftLeft_test) {
    s21_decimal number;

    initDecimal(&number);
    number.bits[LOW] = 1 << 31;
    shiftLeft(&number);
    ck_assert_int_eq(number.bits[MID], 1U);

    initDecimal(&number);
    number.bits[LOW] = 1;
    shiftLeft(&number);
    ck_assert_int_eq(number.bits[LOW], 1U << 1);

    initDecimal(&number);
    number.bits[MID] = 1 << 31;
    shiftLeft(&number);
    ck_assert_int_eq(number.bits[HIGH], 1U);

    initDecimal(&number);
    number.bits[HIGH] = 1 << 31;
    ck_assert_int_eq(shiftLeft(&number), 0);
    ck_assert_int_eq(number.bits[MID], 0);

    // !SEGV or INVALID INPUT
    // ck_assert_int_eq(shiftLeft(NULL), -1);
}
END_TEST

// AB tests
// test growes up scale to 28, afer grows down to 0
// result compare A number and B number (after 56 scale changes)
START_TEST(scaleUpDown_test) {
    int debugNum = -31;
    for (int i = -30; i <= 30; i++) {
        if (debugNum >= -30 && debugNum <= 30) printf("i:%d\n", i);
        s21_decimal decimal;
        initDecimal(&decimal);
        decimal.bits[LOW] = abs(i);
        if (i < 0) set_sign(&decimal, MINUS);

        int j = 0;
        // наращивает скейл
        while (get_scale(decimal) <= 28) {
            // if (i == debugNum) printf("j:%d\n", j);
            if (i == debugNum) printf("curr num:\n");
            // if (i == debugNum) printDecimalBits(decimal, HUMAN_FORMAT);

            if (!scale_up(&decimal)) break;
            if (i == debugNum) printf("after scaleUp:\n");
            // if (i == debugNum) printDecimalBits(decimal, HUMAN_FORMAT);
            if (get_scale(decimal) <= 9) {
                unsigned mustBe = abs(i) * int_pow(10, get_scale(decimal));
                ck_assert_msg(decimal.bits[LOW] == mustBe,
                              "test failed on i = '%d' - error while scaling "
                              "up. decimal[0] is: '%u', scale:%d must be %u",
                              i, decimal.bits[LOW], get_scale(decimal), mustBe);
            }

            if (get_scale(decimal) == 28) break;
            if (!scale_up(&decimal)) break;
            if (i == debugNum) printf("after scaleUp 2:\n");
            // if (i == debugNum) printDecimalBits(decimal, HUMAN_FORMAT);

            scale_down(&decimal, 1, 1);
            if (i == debugNum) printf("after scaleDown:\n");
            // if (i == debugNum) printDecimalBits(decimal, HUMAN_FORMAT);
            if (get_scale(decimal) <= 9) {
                unsigned mustBe = abs(i) * int_pow(10, get_scale(decimal));
                ck_assert_msg(
                    decimal.bits[LOW] == mustBe,
                    "test failed on i = '%d' - error while scaling down AFTER "
                    "UP: decimal[0] is: '%u', scale:%d must be %u",
                    i, decimal.bits[LOW], get_scale(decimal), mustBe);
            }
            j++;
        }

        // сбивает
        while (get_scale(decimal) != 0) {
            if (i == debugNum)
                printf("curr scale:%d\ncurr decimal:\n", get_scale(decimal));
            // if (i == debugNum) printDecimalBits(decimal, HUMAN_FORMAT);
            scale_down(&decimal, 1, 1);
            if (get_scale(decimal) <= 9) {
                unsigned mustBe = abs(i) * int_pow(10, get_scale(decimal));
                ck_assert_msg(
                    decimal.bits[LOW] == mustBe,
                    "test failed on i = '%d' - error while scaling down AFTER "
                    "UP's: decimal[0] is: '%u', scale:%d must be %u",
                    i, decimal.bits[LOW], get_scale(decimal), mustBe);
            }
        }
        ck_assert_msg(
            decimal.bits[LOW] == abs(i) && get_sign(decimal) == (i < 0) ? MINUS
                                                                        : PLUS,
            "test failed on i = '%d' - decimal[0] is: '%u' and not equal to "
            "starting 'i' ",
            i, decimal.bits[LOW]);
    }
}
END_TEST

Suite *utils_suite(void) {
    Suite *suite = suite_create("utils suite");
    TCase *cases = tcase_create("utils case");

    tcase_add_test(cases, initDecimal_test);
    tcase_add_test(cases, getBit_test);
    tcase_add_test(cases, setBit_test);
    tcase_add_test(cases, get_scale_test);
    tcase_add_test(cases, set_scale_test);
    tcase_add_test(cases, shiftRight_test);
    tcase_add_test(cases, shiftLeft_test);
    tcase_add_test(cases, scaleUpDown_test);
    tcase_set_timeout(cases, 999999);
    suite_add_tcase(suite, cases);

    return suite;
}
