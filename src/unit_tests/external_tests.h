#ifndef SRC_UNIT_TESTS_EXTERNAL_TESTS_H_
#define SRC_UNIT_TESTS_EXTERNAL_TESTS_H_
#include <check.h>
#include <math.h>
#include <stdio.h>

Suite *AriphmeticFunctions();
Suite *AddFunctions();
Suite *ChangeFunctions();
#endif  //  SRC_UNIT_TESTS_EXTERNAL_TESTS_H_
