#include "s21_decimal.h"

// get bit in decimal bit filed
// index:
//      0 bit - 95 bit - number`s bits;
//      127 bit - sign bit.
// return values:
//      -1 - overflow
//      0/1 - return bit state
int getBit(s21_decimal num, int index) {
    int returnStatus = -1;

    if (index >= 0 && index <= 127) {
        returnStatus = num.bits[index / 32] & (1u << index % 32);
    }

    if (returnStatus != 0) {
        returnStatus = 1;
    }

    return returnStatus;
}

// Set bit in decimal bit filed
// index:
//      0 bit - 95 bit - number`s bits;
//      127 bit - sign bit.
// values:
//      0;
//      1.
int setBit(s21_decimal* num, int index, int value) {
    int returnStatus = 0;
    if (index >= 0 && index <= 127 && num != NULL) {
        if (value)
            num->bits[index / 32] |= (1u << index % 32);
        else
            num->bits[index / 32] &= ~(1u << index % 32);

        returnStatus = 1;
    }

    return returnStatus;
}

// Logical shift left.
// Return values:
//      1  - OK;
//      0  - overflow has occurred;
//      -1 - null pointer.
int shiftLeft(s21_decimal* num) {
    int returnStatus = 1;
    if (validate(*num)) {
        if (num == NULL) returnStatus = -1;
        // overflow protection
        if (returnStatus == 1 && getBit(*num, 95) == 1) returnStatus = 0;

        if (returnStatus == 1) {
            int tmp1 = 0;
            int tmp2 = 0;

            // part 1 shift
            tmp1 = getBit(*num, 31);
            num->bits[0] <<= 1;

            // part 2
            tmp2 = getBit(*num, 63);
            num->bits[1] <<= 1;
            if (tmp1) num->bits[1] |= 1;

            // part 3
            num->bits[2] <<= 1;
            if (tmp2) num->bits[2] |= 1;
        }
    } else {
        set_value_type(num, s21_NAN);
        returnStatus = false;
    }
    return returnStatus;
}

// Logical shift right.
// Ignores right bits deleting.
// Return values:
//      1  - OK;
//      -1 - null pointer.
int shiftRight(s21_decimal* num) {
    int returnStatus = 1;
    if (validate(*num)) {
        if (num == NULL) returnStatus = -1;

        if (returnStatus == 1) {
            int tmp1 = 0;
            int tmp2 = 0;

            // part 1 shift
            tmp1 = getBit(*num, 64);
            num->bits[2] >>= 1;

            // part 2
            tmp2 = getBit(*num, 32);
            num->bits[1] >>= 1;
            if (tmp1) num->bits[1] |= (1 << 31);

            // part 3
            num->bits[0] >>= 1;
            if (tmp2) num->bits[0] |= (1 << 31);
        }
    } else {
        set_value_type(num, s21_NAN);
        returnStatus = false;
    }
    return returnStatus;
}

// Debug only.
// Print in stdout bit fields
// output_type input values:
//      HUMAN_FORMAT - bits[high] bits[medium] bits[low] bits[scale];
//      MEMORY_FORMAT - bits[low] bits[medium] bits[high] bits[scale].
// void printDecimalBits(s21_decimal num, output_t output_type) {
//     char chr = ' ';
//     char chr2 = '|';
//     char chr0 = '\0';
//     if (output_type == CONVERT_FORMAT) {
//         output_type = HUMAN_FORMAT;
//         chr = '\0';
//         chr2 = '\0';
//         chr0 = '|';
//     }
//     if (output_type == HUMAN_FORMAT) {
//         for (int i = 95; i >= 0; i--) {
//             printf("%d", getBit(num, i));
//             if (i == 64 || i == 32 || i == 0) printf("%c", chr);
//         }
//         printf("%c", chr0);  // debag
//         for (int i = 127; i >= 96; i--) {
//             printf("%d", getBit(num, i));
//             if (i == 127) printf("%c", chr2);  // point sign
//             if (i == 120) printf("%c", chr2);  // point N scale
//             if (i == 112) printf("%c", chr2);  // point N scale
//         }
//     } else {
//         for (int i = 0; i <= 3; i++) {
//             for (int j = 31; j >= 0; j--) {
//                 printf("%d", num.bits[i] & (1 << j) ? 1 : 0);
//                 if ((i == 3) && (j == 31)) printf("|");  // point sign
//                 if ((i == 3) && (j == 24)) printf("|");  // point scale
//                 if ((i == 3) && (j == 16)) printf("|");  // point scale
//             }
//             printf(" ");
//         }
//     }
//     printf("\n");
// }

// Return length from 0 bit to last 1 bit value.
// Example:
//      lastSignificantBit(00010000) return 4.

int lastSignificantBit(s21_decimal num) {
    int result = 0;
    for (result = 95; result >= 0; result--) {
        if (getBit(num, result)) break;
    }

    return result;
}

// ?(camaerie): not sure if this is will be actual normal value
// Zeroing struct s21_decimal.
void initDecimal(s21_decimal* num) {
    num->bits[0] = 0;
    num->bits[1] = 0;
    num->bits[2] = 0;
    num->bits[3] = 0;
    num->value_type = s21_NORMAL_VALUE;
}

// Set sign in decimal.
// sign_t input values:
//      PLUS;
//      MINUS.
void set_sign(s21_decimal* num, sign_t sign) { setBit(num, 127, sign); }

// Return the sign of the value in decimal.
// return signs:
//      PLUS;
//      MINUS.
sign_t get_sign(s21_decimal num) { return (sign_t)getBit(num, 127); }

// Return values:
//      true is struct s21_decimal is empty;
//      false is struct s21_decimal is not empty.
bool is_empty(s21_decimal num) {
    bool is_empty = true;
    if (validate(num)) {
        for (int i = 0; i < 3; i++) {  // mи only fires 3 = ?!
            if (num.bits[i] != 0) is_empty = false;
        }
    } else {
        set_value_type(&num, s21_NAN);
        is_empty = false;
    }
    return is_empty;
}

// SCALE plus

// amount - how many scales to remove
// 1 - divide by 10
// 2 - divide by 100
// 3 - divide by 1000
// returns false if (amount > start scale)
// amount - how many scales to remove
// 1 - divide by 10
// 2 - divide by 100
// 3 - divide by 1000
// returns false if (amount > start scale)
bool scale_down(s21_decimal* num, int amount, int round) {
    bool result = true;
    if (validate(*num)) {
        if (get_scale(*num) < amount || get_scale(*num) == 0 || amount < 0)
            result = false;

        if (result && amount != 0) {
            s21_decimal tens;
            initDecimal(&tens);
            tens.bits[LOW] = 10;
            for (int i = 1; i < amount; i++) {
                scale_up(&tens);
            }
            set_scale(&tens, get_scale(*num));
            int oldScale = get_scale(*num);
            *num = divDecimals(*num, tens, round, NULL);
            set_scale(num, oldScale - amount);
        }
    } else {
        set_value_type(num, s21_NAN);
        result = false;
    }
    return result;
}

// SCALE UP
bool scale_up(s21_decimal* num) {
    s21_decimal num_add1;
    s21_decimal num_add2;
    s21_decimal num_tmp;

    num_add1 = *num;
    num_add2 = *num;

    bool no_error = true;
    int i;
    if (getBit(*num, 95) || getBit(*num, 94) || getBit(*num, 93) ||
        get_scale(*num) == MAX_N_SCALE) {
        no_error = false;
    } else if (validate(*num)) {
        int n_scale = get_scale(*num);
        // num_add1 = num + 0
        shiftLeft(&num_add1);

        // num_add2 = num + 000
        for (i = 0; i < 3; i++) {
            if (n_scale < MAX_N_SCALE) {
                shiftLeft(&num_add2);
            } else {
                no_error = false;
            }
        }

        // add
        num_tmp = s21_add(num_add1, num_add2);
        if (num_tmp.value_type == s21_NORMAL_VALUE) {
            *num = num_tmp;
            set_scale(num, ++n_scale);
        } else {
            no_error = false;
        }
    } else {
        set_value_type(num, s21_NAN);
        no_error = false;
    }

    return no_error;
}

int alignScales(s21_decimal* num1, s21_decimal* num2) {
    int returnStatus = 1;
    bool v1 = 0;
    bool v2 = 0;
    if ((v1 = validate(*num1)) && (v2 = validate(*num2))) {
        int difference = get_scale(*num1) - get_scale(*num2);

        // diff < 0 - increase num1
        // diff > 0 - increase num2
        s21_decimal* numToIncrease;
        s21_decimal* numToDecrease;
        if (difference < 0) {
            numToIncrease = num1;
            numToDecrease = num2;
        } else {
            numToDecrease = num1;
            numToIncrease = num2;
        }

        if (difference < 0) difference *= -1;
        while (difference != 0) {
            if (!scale_up(numToIncrease)) break;
            difference--;
            // printf("A\n");
            // fflush(stdout);
        }
        scale_down(numToDecrease, difference, 1);
    } else {
        if (!v1) set_value_type(num1, s21_NAN);
        if (!v2) set_value_type(num2, s21_NAN);
        returnStatus = false;
    }

    return returnStatus;
}

// returns N scale
int get_scale(s21_decimal num) {
    return ((num.bits[SCALE] & 0xFF0000U) >> 16U);
}

bool set_scale(s21_decimal* num, int n_scale) {
    int error = 404;
    if (validate(*num)) {
        sign_t minus = get_sign(*num);
        if (n_scale <= MAX_N_SCALE && n_scale >= 0) {
            num->bits[SCALE] = (n_scale << 16);
            error = 0;
            setBit(num, 127, minus);
        }
    } else {
        set_value_type(num, s21_NAN);
    }
    return error;
}

// NOT A s21_div
// s21_div are just wrapper around this
// mode - 2 - calculate fraction, change result's scale to fit fraction,
// just s21_div. mode - 1 - return just rounded up result, used for
// scaleDown and s21_div internally. mode - 0 - divide without
// remainder(still sets pointer if needed) and rounding at all. *remainder
// (optional, nullable) - pointer for remainder if needed, literally s21_mod
// maybe.
s21_decimal divDecimals(s21_decimal dividable, s21_decimal divisor, int mode,
                        s21_decimal* remainder) {
    int DEBUG = 0;
    s21_decimal result;
    initDecimal(&result);
    set_scale(&result, 0);

    if (get_scale(dividable) != get_scale(divisor)) {
        if (DEBUG) printf("mode %d aligning scales...\n", mode);
        alignScales(&dividable, &divisor);
        if (DEBUG) {
            printf("mode %d aligned:\n", mode);
            // printDecimalBits(dividable, HUMAN_FORMAT);
            // printDecimalBits(divisor, HUMAN_FORMAT);
        }
    }

    if (divisor.bits[LOW] == 0 && divisor.bits[MID] == 0 &&
        divisor.bits[HIGH] == 0) {
        result.value_type = s21_NAN;
        if (remainder != NULL) {
            initDecimal(remainder);
            remainder->value_type = s21_NAN;
        }

    } else {
        int resultSign;
        int remainderSign;
        if (get_sign(dividable) != get_sign(divisor))
            resultSign = MINUS;
        else
            resultSign = PLUS;

        remainderSign = get_sign(dividable);
        // set plus for correct division
        set_sign(&dividable, PLUS);
        set_sign(&divisor, PLUS);

        int currDividableIndex = lastSignificantBit(dividable);
        s21_decimal currNumToSub;
        initDecimal(&currNumToSub);
        set_scale(&currNumToSub, get_scale(dividable));

        while (currDividableIndex >= 0) {
            // take next bit from dividable
            shiftLeft(&currNumToSub);
            setBit(&currNumToSub, 0, getBit(dividable, currDividableIndex));
            currDividableIndex--;

            if (cmpDecimals(currNumToSub, divisor) >= 0) {
                currNumToSub = s21_sub(currNumToSub, divisor);
                shiftLeft(&result);
                setBit(&result, 0, 1);
            } else {
                shiftLeft(&result);
                setBit(&result, 0,
                       0);  // not actually need, but feels right
            }
        }

        // currNumToSub means remainder from now on

        s21_decimal zero;
        initDecimal(&zero);
        set_scale(&zero, get_scale(currNumToSub));
        if (remainder != NULL) {
            *remainder = currNumToSub;
            set_sign(remainder, remainderSign);
        }
        if (DEBUG) {
            printf("mode %d base divided, result:\n", mode);
            // printDecimalBits(result, HUMAN_FORMAT);
        }
        if (s21_is_greater(currNumToSub, zero) == 0 && mode != 0) {
            switch (mode) {  // mode 0 checked above
                case 1: {    // round algorithm
                    set_scale(&currNumToSub, 0);
                    scale_up(&currNumToSub);
                    set_scale(&currNumToSub, 0);

                    s21_decimal remainderOfRemainder;
                    set_scale(&divisor, 0);
                    if (DEBUG) {
                        printf("mode 1 to mode 0...\n");
                    }
                    s21_decimal scaledUpRemainder = divDecimals(
                        currNumToSub, divisor, 0, &remainderOfRemainder);

                    s21_decimal five;
                    initDecimal(&five);
                    five.bits[LOW] = 5;
                    // if more than 5
                    // or 5 and have some value left
                    // or 5 but no value left BUT prev digit is
                    // odd(basically checks integer part for oddity in that
                    // case)
                    if ((cmpDecimals(scaledUpRemainder, five) == 1) ||
                        (cmpDecimals(scaledUpRemainder, five) == 0 &&
                         cmpDecimals(remainderOfRemainder, zero) != 0) ||
                        (cmpDecimals(scaledUpRemainder, five) == 0 &&
                         cmpDecimals(remainderOfRemainder, zero) == 0 &&
                         getBit(result, 0) == 1)) {
                        s21_decimal one;
                        initDecimal(&one);
                        one.bits[LOW] = 1;
                        set_scale(&one, get_scale(result));
                        result = s21_add(result, one);
                    }
                    break;
                }
                case 2: {  // just s21_div
                    set_scale(&currNumToSub, 0);

                    s21_decimal maxedScaleInt = result;
                    while (true)
                        if (!scale_up(&maxedScaleInt)) break;

                    int scales_left = get_scale(maxedScaleInt);
                    int total_scales_need = get_scale(maxedScaleInt);

                    s21_decimal fraction;
                    initDecimal(&fraction);

                    s21_decimal tmpFraction;
                    initDecimal(&tmpFraction);

                    set_scale(&fraction, total_scales_need);
                    set_scale(&tmpFraction, total_scales_need);
                    set_scale(&divisor, 0);

                    if (DEBUG) printf("MODE 2 FRACTION CALCULATION START\n");
                    while (scales_left > 0) {
                        if (DEBUG) {
                            printf("NEW ROUND. SCALES LEFT:%d\n", scales_left);
                            printf("CURR REMAINDER TO SCALE UP:\n");
                            // printDecimalBits(currNumToSub, HUMAN_FORMAT);
                            printf("CURR TOTAL FRACTION:\n");
                            // printDecimalBits(fraction, HUMAN_FORMAT);
                            printf("BEGIN SCALING UP:\n\n");
                        }

                        while (get_scale(currNumToSub) < scales_left) {
                            if (!scale_up(&currNumToSub)) break;
                        }

                        if (DEBUG) {
                            printf("SCALED UP REMAINDER, SCALES TO REMOVE:%d\n",
                                   get_scale(currNumToSub));
                            // printDecimalBits(currNumToSub, HUMAN_FORMAT);
                        }

                        if (get_scale(currNumToSub) == 0) {
                            if (DEBUG) printf("BREAK");
                            break;
                        }

                        set_scale(&divisor, get_scale(currNumToSub));
                        if (scales_left !=
                            get_scale(currNumToSub)) {  // if last iteration
                                                        // then rounding
                            tmpFraction = divDecimals(currNumToSub, divisor, 0,
                                                      &currNumToSub);
                        } else {
                            tmpFraction = divDecimals(currNumToSub, divisor, 1,
                                                      &currNumToSub);
                        }
                        set_scale(&currNumToSub, 0);  // for text iteration
                        set_scale(
                            &tmpFraction,
                            get_scale(
                                divisor));  // scale restore after division
                        int origTmpFracScale = get_scale(tmpFraction);
                        // shift number to put digits in right place of fraction
                        while ((scales_left - get_scale(tmpFraction)) != 0) {
                            if (!scale_up(&tmpFraction)) {
                                break;
                            }
                        }
                        scales_left -= origTmpFracScale;
                        set_scale(&tmpFraction,
                                  total_scales_need);  // just make sure
                        fraction = s21_add(fraction, tmpFraction);
                    }

                    // minimize scale until precision loss
                    s21_decimal remainderTmp = fraction;
                    while (true) {
                        if (!scale_down(&remainderTmp, 1, 1)) break;
                        // printf("minimized fraction:\n");
                        // printDecimalBits(remainderTmp, HUMAN_FORMAT);
                        scale_up(&remainderTmp);
                        if (cmpDecimals(fraction, remainderTmp) == 0) {
                            scale_down(&fraction, 1, 1);
                            remainderTmp = fraction;
                        } else {
                            break;
                        }
                    }
                    result = s21_add(result, fraction);
                    // minimize result until precision loss
                    s21_decimal resultTmp = result;
                    while (true) {
                        if (!scale_down(&resultTmp, 1, 1)) break;
                        scale_up(&resultTmp);
                        if (cmpDecimals(result, resultTmp) == 0) {
                            scale_down(&result, 1, 1);
                            resultTmp = result;
                        } else {
                            break;
                        }
                    }
                } break;
            }
        }
        set_sign(&result, resultSign);
    }

    return result;
}

// uses alignScales
// s21 comparing funcitons can be wrappers around this
// @retval -1 - num1 < num2
// @retval 0 - num1 = num2
// @retval 1 - num1 > num2
int cmpDecimals(s21_decimal num1, s21_decimal num2) {
    if (get_scale(num1) != get_scale(num2)) {
        alignScales(&num1, &num2);
    }

    int result = -2;

    // if signs are different
    if (get_sign(num1) != get_sign(num2)) {
        if (get_sign(num1) == MINUS)
            result = -1;
        else
            result = 1;
    }

    if (result == -2) {
        for (int i = 95; i >= 0; i--) {
            if (getBit(num1, i) && !getBit(num2, i)) {
                result = 1;
                break;
            }
            if (!getBit(num1, i) && getBit(num2, i)) {
                result = -1;
                break;
            }
        }
        if (result == -2) result = 0;
        // if numbers are neganive, invert result
        else if (get_sign(num1) == MINUS && get_sign(num2) == MINUS)
            result *= -1;
    }

    return result;
}

/**
 * @brief convert string representation of number
 * to int, int overflow don`t handle
 * @param string string digit of number
 * @returns (int) integer representation of string number
 */
int string_to_int(const char* string) {
    int res_number;
    int sign;
    int i;

    if (string[0] == '+') {
        sign = 1;
        ++string;
    } else if (string[0] == '-') {
        sign = -1;
        ++string;
    } else {
        sign = 1;
    }

    for (res_number = 0, i = 0; string[i] >= '0' && string[i] <= '9'; ++i) {
        res_number = res_number * 10 + (string[i] - '0');
    }

    return res_number * sign;
}

bool validate(s21_decimal src) {
    bool success = false;

    // if infinity
    // and bits non - empty
    int val = src.value_type;
    if (val == 1 || val == 2) {
        if (src.bits[LOW] == 0 && src.bits[MID] == 0 && src.bits[HIGH] == 0 &&
            src.bits[SCALE] == 0) {
            success = true;
        }
    }

    if (success == false && ((src.bits[SCALE] & ~0x80FF0000) == 0) &&
        (get_scale(src) <= 28))
        success = true;

    if (val == 3) success = false;
    return success;
}

bool set_value_type(s21_decimal* dec, int value) {
    bool abnormal = false;
    initDecimal(dec);
    dec->value_type = value;
    if (value != s21_NORMAL_VALUE) abnormal = true;
    return abnormal;
}

int add_value_types(s21_decimal dec_1, s21_decimal dec_2) {
    int value1 = dec_1.value_type;
    int value2 = dec_2.value_type;
    int values = s21_NORMAL_VALUE;
    if (value1 || value2) {
        if ((value1 == s21_INFINITY && value2 != s21_NEGATIVE_INFINITY) ||
            (value2 == s21_INFINITY && value1 != s21_NEGATIVE_INFINITY))
            values = s21_INFINITY;
        if ((value1 != s21_INFINITY && value2 == s21_NEGATIVE_INFINITY) ||
            (value2 != s21_INFINITY && value1 == s21_NEGATIVE_INFINITY))
            values = s21_NEGATIVE_INFINITY;
        if (value1 == s21_NAN || value2 == s21_NAN ||
            (value1 == s21_INFINITY && value2 == s21_NEGATIVE_INFINITY) ||
            (value2 == s21_INFINITY && value1 == s21_NEGATIVE_INFINITY))
            values = s21_NAN;
    }

    return values;
}

int sub_value_types(s21_decimal dec_1, s21_decimal dec_2) {
    int value1 = dec_1.value_type;
    int value2 = dec_2.value_type;
    int values = s21_NORMAL_VALUE;
    if (value1 || value2) {
        if ((value1 == s21_INFINITY && value2 != s21_NEGATIVE_INFINITY) ||
            (value1 != s21_NEGATIVE_INFINITY &&
             value2 == s21_NEGATIVE_INFINITY))
            values = s21_INFINITY;

        if ((value1 == s21_NEGATIVE_INFINITY && value2 != s21_INFINITY) ||
            (value1 != s21_NEGATIVE_INFINITY && value2 == s21_INFINITY))
            values = s21_NEGATIVE_INFINITY;

        if (value1 == s21_NAN || value2 == s21_NAN ||
            (value1 == s21_INFINITY && value2 == s21_INFINITY) ||
            (value1 == s21_NEGATIVE_INFINITY &&
             value2 == s21_NEGATIVE_INFINITY) ||
            (value1 == s21_NEGATIVE_INFINITY && value2 == s21_INFINITY) ||
            (value1 == s21_INFINITY && value2 == s21_NEGATIVE_INFINITY))
            values = s21_NAN;
    }
    return values;
}

int div_value_types(s21_decimal dec_1, s21_decimal dec_2) {
    int value1 = dec_1.value_type;
    int value2 = dec_2.value_type;
    int values = s21_NORMAL_VALUE;
    int sign2 = get_sign(dec_2);
    if (value1 || value2) {
        // result = 0
        if ((value1 == s21_NORMAL_VALUE &&
             (value2 == s21_INFINITY || value2 == s21_NEGATIVE_INFINITY)))
            values = 4;  // mean result has s21_normal_value and all bits = 0

        // result = infinity
        if ((value1 == s21_INFINITY && value2 == s21_NORMAL_VALUE &&
             sign2 == PLUS) ||
            (value1 == s21_NEGATIVE_INFINITY && value2 == s21_NORMAL_VALUE &&
             sign2 == MINUS))
            values = s21_INFINITY;

        // result = -infinity
        if ((value1 == s21_NEGATIVE_INFINITY && value2 == s21_NORMAL_VALUE &&
             sign2 == PLUS) ||
            (value1 == s21_INFINITY && value2 == s21_NORMAL_VALUE &&
             sign2 == MINUS))
            values = s21_NEGATIVE_INFINITY;

        // result = not a number
        if ((value1 == s21_INFINITY && value2 == s21_NEGATIVE_INFINITY) ||
            (value1 == s21_NEGATIVE_INFINITY && value2 == s21_INFINITY) ||
            (value1 == s21_INFINITY && value2 == s21_INFINITY) ||
            (value1 == s21_NEGATIVE_INFINITY &&
             value2 == s21_NEGATIVE_INFINITY) ||
            (value1 == s21_NAN || value2 == s21_NAN) ||
            (value2 == s21_NORMAL_VALUE && is_empty(dec_2) == true))

            values = s21_NAN;
    }
    return values;
}
